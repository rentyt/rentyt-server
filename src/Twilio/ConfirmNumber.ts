import { Router } from "express";
import userSchema from "../models/user";
import dotenv from "dotenv";

dotenv.config({ path: "variables.env" });

const routerPhone = Router();

const accountSid = process.env.accountSid;
const authToken = process.env.authToken;
const SERVICESID = process.env.SERVICESID;

const client = require("twilio")(accountSid, authToken, SERVICESID);

routerPhone.post("/verify-phone", async (req, res) => {
  const { phone } = req.body;
  const channel = "sms";
  await client.verify
    .services(SERVICESID)
    .verifications.create({ to: `${phone}`, channel })
    .then((message: any) => {
      res.status(200).json({ success: true, data: message }).end();
    })
    .catch((err: any) => {
      console.log("Err sen message", err);
      res.status(404).json({ success: false, data: err }).end();
    });
});

routerPhone.post("/verify-code", async (req, res) => {
  const { phone, code, id } = req.body;
  client.verify
    .services(SERVICESID)
    .verificationChecks.create({ to: `${phone}`, code: code })
    .then((verification_check: any) => {
      console.log(verification_check);
      if (verification_check.status === "approved") {
        userSchema.findOneAndUpdate(
          { _id: id },
          {
            $set: { verifyPhone: true, phone: phone },
          },
          //@ts-ignore
          (err, user) => {
            if (err) {
              res.status(200).json({ success: false, data: err }).end();
            } else {
              res
                .status(200)
                .json({ success: true, data: verification_check })
                .end();
            }
          }
        );
      } else {
        res
          .status(200)
          .json({ success: false, data: verification_check })
          .end();
      }
    })
    .catch((err: any) => {
      res.status(200).json({ success: false, data: err }).end();
    });
});

routerPhone.post("/send-sms-to-client", (req, res) => {
  const { telefono, mensageMovil } = req.body;
  client.messages
    .create({
      body: mensageMovil,
      from: "RENTYT",
      to: `+${telefono}`,
    })
    .then((message: any) =>
      res.json({ data: "Mensaje enviado con éxito", success: true }).end()
    )
    .catch((err: any) =>
      res.json({ data: "Algo va mal intentalo de nuevo", success: false }).end()
    );
});

export default routerPhone;
