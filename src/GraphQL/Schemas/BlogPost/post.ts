import { gql } from "apollo-server-express";
// Models
import postSchema from "../../../models/post";

export const typeDefs = gql`
  type Post {
    _id: ID
    title: String
    short_description: String
    author: JSON
    image: String
    tags: [String]
    slug: String
    content: String
    like: [String]
    created_at: Date
    updated_at: Date
    related: String
  }

  type postResponse {
    success: Boolean
    message: String
    data: [Post]
  }

  type postSlugResponse {
    success: Boolean
    message: String
    data: Post
  }

  type createdPostResponse {
    success: Boolean
    message: String
  }

  extend type Query {
    getPost(page: Int, limit: Int): postResponse
    getPostForSlug(slug: String): postSlugResponse
    getRelatedPost(tags: String, page: Int, limit: Int): postResponse
  }

  extend type Mutation {
    updatePost(id: ID, like: String): createdPostResponse
  }
`;

export const resolvers = {
  Query: {
    getPost: async (root: any, { page, limit }) => {
      return new Promise((resolve, reject) => {
        postSchema
          .find((error: any, post: any) => {
            if (error) {
              reject({
                success: false,
                message: "There is a problem with your request",
                data: [],
              });
            } else {
              resolve({
                success: true,
                message: "Successful operation",
                data: post,
              });
            }
          })
          .sort({ $natural: -1 })
          .limit(limit * 1)
          .skip((page - 1) * limit)
          .exec();
      });
    },

    getPostForSlug: async (root: any, { slug }) => {
      return new Promise((resolve, reject) => {
        postSchema.findOne({ slug: slug }, (error: any, post: any) => {
          if (error) {
            reject({
              success: false,
              message: "There is a problem with your request",
              data: null,
            });
          } else {
            resolve({
              success: true,
              message: "Successful operation",
              data: post,
            });
          }
        });
      });
    },

    getRelatedPost: async (root: any, { tags, page, limit }) => {
      return new Promise((resolve, reject) => {
        postSchema
          .find(
            {
              tags: {
                $in: tags,
              },
            },
            (error: any, post: any) => {
              if (error) {
                reject({
                  success: false,
                  message: "There is a problem with your request",
                  data: [],
                });
              } else {
                resolve({
                  success: true,
                  message: "Successful operation",
                  data: post,
                });
              }
            }
          )
          .sort({ $natural: -1 })
          .limit(limit * 1)
          .skip((page - 1) * limit)
          .exec();
      });
    },
  },

  Mutation: {
    updatePost: async (root: any, { id, like }) => {
      console.log(id, like);
      return new Promise((resolve, object) => {
        postSchema.findOneAndUpdate(
          { _id: id },
          {
            $push: {
              like: like,
            },
          },
          { new: true },
          (error) => {
            if (error) {
              object({
                success: false,
                message: "Hubo un problema con su solicitud",
              });
            } else {
              resolve({
                success: true,
                message: "Post publicado con éxito",
              });
            }
          }
        );
      });
    },
  },
};
