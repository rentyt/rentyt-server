import cuponSchema from "../../../models/cupones";
import adsSchema from "../../../models/ads";
import userSchema from "../../../models/user";
import orderSchema from "../../../models/order";
import { gql } from "apollo-server-express";

export const typeDefs = gql`
  type CuponResponse {
    success: Boolean
    messages: String
    data: [Cupon]
  }

  type Cupon {
    _id: ID
    clave: String!
    descuento: Int
    tipo: String
    usage: Int
    expire: Date
    exprirable: Boolean
    user: String
    description: String
    private: Boolean
    uniqueCar: Boolean
    canjeado: Boolean
    vencido: Boolean
    car: String
    Car: JSON
    Usuario: JSON
  }

  input cuponInput {
    data: JSON
  }

  extend type Query {
    getPromocode(input: JSON): CuponResponse
  }

  extend type Mutation {
    crearCupon(input: cuponInput): Cupon
    eliminarCupon(id: ID): generarResponse
  }
`;

export const resolvers = {
  Mutation: {
    crearCupon: async (root: any, { input }) => {
      try {
        const nuevoCupon = new cuponSchema(input.data);
        return new Promise((resolve, reject) => {
          nuevoCupon.save(async (error, cupon) => {
            if (error) {
              return reject(error);
            } else {
              /*  if (input.private) {
                const user = await userSchema.findOne({ _id: input.user });
                sendNotification(user?.OnesignalID, input.description, null);
              } */
              return resolve(cupon);
            }
          });
        });
      } catch (error) {
        return new Promise((_resolve, reject) => {
          return reject({
            success: false,
            message: "Hay un problema con su solicitud",
            data: null,
          });
        });
      }
    },

    eliminarCupon: (root: any, { id }) => {
      return new Promise((resolve, reject) => {
        //@ts-ignore
        cuponSchema.findOneAndDelete({ _id: id }, (error: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
            });
          else
            resolve({
              success: true,
              message: "Miembro eliminado con éxito",
            });
        });
      });
    },
  },

  Query: {
    getPromocode: (root: any, { input }) => {
      return new Promise((resolve, rejects) => {
        cuponSchema.find(input, (err: any, res: any) => {
          if (err) {
            rejects({
              messages: "Algo salio mal intentalo de nuevo",
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: "Cupones extraido con exito",
              success: true,
              data: res,
            });
          }
        });
      });
    },
  },

  Cupon: {
    Car(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        adsSchema.findOne({ _id: parent.car }, (error: any, ads: any) => {
          if (error) {
            rejects(null);
          } else {
            resolve(ads);
          }
        });
      });
    },

    Usuario(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.user }, (error: any, user: any) => {
          if (error) {
            rejects(null);
          } else {
            resolve(user);
          }
        });
      });
    },

    canjeado(parent: any, args: any, { usuarioActual }) {
      return new Promise((resolve, rejects) => {
        orderSchema.findOne(
          {
            customer: usuarioActual ? usuarioActual._id : null,
            cupon: parent.id,
            status: { $ne: "Pendiente de pago" },
          },
          (error, user) => {
            if (error) {
              rejects(error);
            } else {
              if (!user) resolve(false);
              else {
                resolve(true);
              }
            }
          }
        );
      });
    },
    vencido(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        if (parent.exprirable) {
          if (parent.expire < new Date()) {
            resolve(true);
          } else {
            resolve(false);
          }
        } else {
          resolve(false);
        }
      });
    },
  },
};
