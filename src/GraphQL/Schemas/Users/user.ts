import bcrypt from "bcryptjs";
import { crearToken } from "../../createToken";
import { createStripeClient } from "../../../libs/CreateStripeClient";
import userSchema from "../../../models/user";
import ratingSchema from "../../../models/rating";
import orderSchema from "../../../models/order";
import carSchema from "../../../models/ads";
import { SaveEmailToMailJet } from "../../../Mail/createContact";
import { welcomeEmail } from "../../../Emails/Welcome/sendEmail";
import { gql } from "apollo-server-express";
import { updateUserLogin } from "../../../Funtions/AdsUpdate";
import { LoginEmail } from "../../../Emails/Welcome/LoginEmail";

export const typeDefs = gql`
  type OauthUserResponse {
    success: Boolean!
    message: String
    data: OauthUser
  }

  type OauthUser {
    token: String!
    id: String!
    verifyPhone: Boolean
    user: JSON
  }

  type CrearUsuarioResponse {
    success: Boolean!
    message: String
    data: Usuario
  }

  type userResponse {
    success: Boolean!
    message: String
    status: Int
    data: Usuario
  }

  type Usuario {
    _id: String
    name: String
    lastName: String
    email: String
    city: String
    avatar: String
    phone: String
    created_at: Date
    updated_at: Date
    termAndConditions: Boolean
    verifyPhone: Boolean
    isAvalible: Boolean
    StripeID: String
    PaypalID: String
    OnesignalID: String
    socialNetworks: String
    isSocial: Boolean
    rating: String
    contactCode: String
    verified: Boolean
    myCategory: [String]
    lastSeen: Date
    description: String
    totalOrder: Int
    totalRating: Int
    totalCar: Int
    location: JSON
    additionalAddress: String
    documents: JSON
    lastLogin: JSON
  }

  input UsuarioInput {
    name: String
    lastName: String
    email: String
    password: String
    termAndConditions: Boolean
    telefono: String
    isAvalible: Boolean
    city: String
    contactCode: String
    verified: Boolean
    myCategory: [String]
    lastSeen: Date
  }

  extend type Query {
    getUser(id: ID): userResponse
    getUserLogin: userResponse
  }

  extend type Mutation {
    LoginUser(email: String, password: String, input: JSON): OauthUserResponse
    crearUsuario(input: UsuarioInput): CrearUsuarioResponse
    updateUser(input: JSON): userResponse
  }
`;

export const resolvers = {
  Mutation: {
    LoginUser: async (root: any, { email, password, input }) => {
      const users = await userSchema.findOne({ email });
      if (!users) {
        return {
          success: false,
          message: "noUser",
          data: null,
        };
      } else {
        updateUserLogin(users._id, input);
        const passwordCorrecto = await bcrypt.compare(password, users.password);
        if (users.isAvalible) {
          if (!passwordCorrecto) {
            return {
              success: false,
              message: "ErroMessage",
              data: null,
            };
          } else {
            LoginEmail(email, users, input);
            return {
              success: true,
              message: "successMessage",
              data: {
                token: crearToken(users, process.env.SECRETO, "9999 years"),
                id: users._id,
                user: users,
                verifyPhone: users.verifyPhone,
              },
            };
          }
        } else {
          return {
            success: false,
            message: "DisabledErroMessage",
            data: null,
          };
        }
      }
    },

    crearUsuario: async (root: any, { input }) => {
      const emailExists = await userSchema.findOne({ email: input.email });
      if (emailExists) {
        return {
          success: false,
          message: "existsMessage",
          data: null,
        };
      }

      const nuevoUsuario = new userSchema({
        name: input.name,
        lastName: input.lastName,
        email: input.email,
        password: input.password,
        termAndConditions: false,
        myCategory: input.myCategory,
        lastSeen: new Date(),
      });

      return new Promise((resolve, object) => {
        nuevoUsuario.save(async (error) => {
          if (error)
            object({
              success: false,
              message: "Hubo un problema con su solicitud",
              data: null,
            });
          else {
            createStripeClient(
              nuevoUsuario.name,
              nuevoUsuario.email,
              nuevoUsuario._id
            );

            SaveEmailToMailJet(
              nuevoUsuario.email,
              nuevoUsuario.name,
              nuevoUsuario.lastName
            );

            welcomeEmail(nuevoUsuario.email);

            resolve({
              success: true,
              message: "Bienvenido a Rentyt",
              data: nuevoUsuario,
            });
          }
        });
      });
    },

    updateUser: async (root: any, { input }) => {
      if (input._id) {
        return new Promise((_resolve, reject) => {
          userSchema.findOneAndUpdate(
            { _id: input._id },
            input,
            { new: true },
            (err, user) => {
              if (err) {
                return reject({
                  message: "Algo salio mal intentalo de nuevo",
                  success: false,
                  status: 400,
                  data: null,
                });
              } else {
                return _resolve({
                  message: "Cuenta actualizada con éxito",
                  success: true,
                  status: 200,
                  data: user,
                });
              }
            }
          );
        });
      } else {
        return new Promise((resolve, reject) => {
          return reject({
            message: "Algo salio mal intentalo de nuevo",
            success: false,
            status: 400,
            data: null,
          });
        });
      }
    },
  },

  Query: {
    getUser: (root: any, { id }) => {
      return new Promise((resolve, reject) => {
        userSchema.findOne({ _id: id }, (error: any, user: any) => {
          if (error) {
            reject({
              success: false,
              message: "There is a problem with your request",
              status: 404,
              data: null,
            });
          } else {
            resolve({
              success: true,
              message: "Successful operation",
              status: 200,
              data: user,
            });
          }
        });
      });
    },

    getUserLogin: (root: any, {}, { usuarioActual }) => {
      if (!usuarioActual) {
        return {
          success: false,
          messages: "Debes iniciar sesión para continuar",
          status: 404,
          data: null,
        };
      } else {
        return new Promise((resolve, reject) => {
          userSchema.findOne(
            { _id: usuarioActual._id },
            (error: any, user: any) => {
              if (error) {
                reject({
                  success: false,
                  message: "There is a problem with your request",
                  status: 404,
                  data: null,
                });
              } else {
                resolve({
                  success: true,
                  message: "Successful operation",
                  status: 200,
                  data: user,
                });
              }
            }
          );
        });
      }
    },
  },

  Usuario: {
    totalOrder(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        orderSchema.find({ owner: parent._id }, (error: any, order: any) => {
          if (error) {
            rejects(0);
          } else {
            resolve(order.length);
          }
        });
      });
    },
    totalRating(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        ratingSchema.find({ owner: parent._id }, (error: any, rating: any) => {
          if (error) {
            rejects(0);
          } else {
            resolve(rating.length);
          }
        });
      });
    },
    totalCar(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        carSchema.find({ owner: parent._id }, (error: any, ads: any) => {
          if (error) {
            rejects(0);
          } else {
            resolve(ads.length);
          }
        });
      });
    },
  },
};
