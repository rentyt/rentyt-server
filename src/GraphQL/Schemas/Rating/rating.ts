import ratingSchema from "../../../models/rating";
import userSchema, { IUser } from "../../../models/user";
import adsSchema, { IAds } from "../../../models/ads";
import ordersSchema from "../../../models/order";
import { pushNotificationNewOrder } from "../../../Funtions/NotificationNEWORDER";
import { gql } from "apollo-server-express";
import {
  updateCarRating,
  updateUserRating,
  UpdateOrderStatus,
} from "../../../Funtions/AdsUpdate";

export const typeDefs = gql`
  type ratingResponse {
    success: Boolean!
    message: String!
    data: [Rating]
  }

  type Rating {
    _id: ID
    owner: String
    customer: String
    car: String
    order: String
    rating: JSON
    comment: String
    reply: JSON
    Customer: Usuario
    Owner: Usuario
    Car: Ads
    created_at: Date
    updated_at: Date
  }

  input RatingInput {
    data: JSON
  }

  extend type Mutation {
    crearRating(input: RatingInput): generarResponse
    updateRating(input: RatingInput): generarResponse
  }

  extend type Query {
    getRating(owner: ID, limit: Int, page: Int): ratingResponse
  }
`;

export const resolvers = {
  Mutation: {
    crearRating: async (root: any, { input }) => {
      try {
        const nuevoRating = new ratingSchema(input.data);
        return new Promise((resolve, reject) => {
          nuevoRating.save(async (error) => {
            if (error) {
              return reject({
                messages: "Algo salio mal intentalo de nuevo",
                success: false,
                status: 400,
              });
            } else {
              const owner = await userSchema.findOne({ _id: input.data.owner });
              const order = await ordersSchema.findOne({
                _id: input.data.order,
              });
              updateUserRating(input.data.owner);
              updateCarRating(input.data.car);
              UpdateOrderStatus(input.data.order);
              pushNotificationNewOrder(owner.OnesignalID, "Finalizada", order);
              return resolve({
                messages: "Tu valoración ha sido enviada",
                success: true,
                status: 200,
              });
            }
          });
        });
      } catch (error) {
        return new Promise((_resolve, reject) => {
          return reject({
            messages: "Algo salio mal intentalo de nuevo",
            success: false,
            status: 401,
          });
        });
      }
    },

    updateRating: async (root: any, { input }) => {
      return new Promise((_resolve, reject) => {
        ratingSchema.findOneAndUpdate(
          { _id: input.data._id },
          input.data,
          { upsert: true },
          (err, data) => {
            if (err) {
              return reject({
                messages: "Algo salio mal intentalo de nuevo",
                success: false,
                status: 400,
              });
            } else {
              return _resolve({
                messages: "Tu respuesta ha sido enviada",
                success: true,
                status: 200,
              });
            }
          }
        );
      });
    },
  },

  Query: {
    getRating: (root: any, { owner, limit, page }) => {
      return new Promise((resolve, reject) => {
        ratingSchema
          .find(
            {
              owner: owner,
            },
            (error: any, order: any) => {
              if (error) {
                reject({
                  success: false,
                  message: "There is a problem with your request",
                  data: [],
                });
              } else {
                resolve({
                  success: true,
                  message: "Successful operation",
                  data: order,
                });
              }
            }
          )
          .sort({ $natural: -1 })
          .limit(limit * 1)
          .skip((page - 1) * limit)
          .exec();
      });
    },
  },

  Rating: {
    Customer(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne(
          { _id: parent.customer },
          (error: any, user: IUser) => {
            if (error) {
              rejects(null);
            } else {
              resolve(user);
            }
          }
        );
      });
    },

    Owner(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.owner }, (error: any, user: IUser) => {
          if (error) {
            rejects(null);
          } else {
            resolve(user);
          }
        });
      });
    },

    Car(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        adsSchema.findOne({ _id: parent.car }, (error: any, ads: IAds) => {
          if (error) {
            rejects(null);
          } else {
            resolve(ads);
          }
        });
      });
    },
  },
};
