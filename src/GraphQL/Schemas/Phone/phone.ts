import { gql } from "apollo-server-express";
// Models
import phoneSchema from "../../../models/Phone";

export const typeDefs = gql`
  type Phone {
    _id: ID
    name: String
    code: String
    dial_code: String
    flag: String
  }

  type PhoneResponse {
    success: Boolean
    message: String
    data: [Phone]
  }

  type PhoneDefaultResponse {
    success: Boolean
    message: String
    data: Phone
  }

  extend type Query {
    getPhone(search: String): PhoneResponse
    getPhoneCode(code: String): PhoneDefaultResponse
  }
`;

export const resolvers = {
  Query: {
    getPhone: async (root: any, { search }) => {
      let condition = {};

      if (search) condition = { $text: { $search: `"\"${search} \""` } };

      return new Promise((resolve, reject) => {
        phoneSchema.find(condition, (error: any, post: any) => {
          if (error) {
            reject({
              success: false,
              message: "There is a problem with your request",
              data: [],
            });
          } else {
            resolve({
              success: true,
              message: "Successful operation",
              data: post,
            });
          }
        });
      });
    },

    getPhoneCode: async (root: any, { code }) => {
      return new Promise((resolve, reject) => {
        phoneSchema.findOne({ code: code }, (error: any, phone: any) => {
          if (error) {
            reject({
              success: false,
              message: "There is a problem with your request",
              data: null,
            });
          } else {
            resolve({
              success: true,
              message: "Successful operation",
              data: phone,
            });
          }
        });
      });
    },
  },
};
