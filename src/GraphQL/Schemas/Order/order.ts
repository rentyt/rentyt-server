import { gql } from "apollo-server-express";
import newOrderSchema from "../../../models/order";
import AdsSchema, { IAds } from "../../../models/ads";
import userSchema, { IUser } from "../../../models/user";
import { IOrders } from "../../../models/order";
import { UpdateBusyCar } from "../../../Funtions/AdsUpdate";
import { pushNotificationOrderProccess } from "../../../Funtions/NotificationStatusOrder";
import { pushNotificationNewOrder } from "../../../Funtions/NotificationNEWORDER";

export const typeDefs = gql`
  type newOrderResponse {
    success: Boolean
    message: String
    status: Int
    data: Order
  }

  type newOrderAllResponse {
    success: Boolean
    message: String
    status: Int
    count: Int
    data: [Order]
  }

  type Order {
    _id: ID
    channelOrderDisplayId: Int
    pickupDate: JSON
    deliveryDate: JSON
    customer: String
    owner: String
    car: String
    orderIsAlreadyPaid: Boolean
    payment: JSON
    note: String
    IntegerValue: Int
    paymentMethod: String
    cupon: ID
    statusProcess: JSON
    pagoPaypal: JSON
    stripePaymentIntent: JSON
    status: String
    holdedPartnerID: String
    invoiceUrl: String
    selectdDay: JSON
    Owner: Usuario
    Customer: Usuario
    Car: Ads
    created_at: Date
    updated_at: Date
    airplanePickUp: Boolean
    extraEquipament: JSON
    babyChair: JSON
    instantBooking: Boolean
    airPort: String
    source: JSON
  }

  input OrderInput {
    data: JSON
  }

  extend type Query {
    getAllOrder(user: ID, status: [String]): newOrderAllResponse
    getOrder(id: ID): newOrderResponse
    getAllRequest(
      owner: ID
      status: [String]
      page: Int
      limit: Int
    ): newOrderAllResponse
  }

  extend type Mutation {
    crearModificarNewOrden(input: OrderInput): newOrderResponse
    NewOrdenProceed(
      ordenId: ID
      status: String
      IntegerValue: Int
    ): generarResponse
  }
`;

export const resolvers = {
  Query: {
    getAllOrder: (root: any, { user, status }) => {
      return new Promise((resolve, reject) => {
        newOrderSchema
          .find(
            {
              customer: user ? user : null,
              status: {
                $in: status,
              },
            },
            (error: any, order: any) => {
              if (error) {
                reject({
                  success: false,
                  message: "There is a problem with your request",
                  status: 404,
                  data: [],
                });
              } else {
                resolve({
                  success: true,
                  message: "Successful operation",
                  status: 200,
                  data: order,
                });
              }
            }
          )
          .sort({ $natural: -1 })
          .exec();
      });
    },

    getAllRequest: (root: any, { owner, status, page, limit }) => {
      return new Promise((resolve, reject) => {
        newOrderSchema
          .find(
            {
              owner: owner ? owner : null,
              status: {
                $in: status,
              },
            },
            (error: any, order: any) => {
              if (error) {
                reject({
                  success: false,
                  message: "There is a problem with your request",
                  status: 404,
                  data: [],
                });
              } else {
                resolve({
                  success: true,
                  message: "Successful operation",
                  status: 200,
                  data: order,
                });
              }
            }
          )
          .sort({ $natural: -1 })
          .limit(limit * 1)
          .skip((page - 1) * limit)
          .exec();
      });
    },
    getOrder: (root: any, { id }) => {
      return new Promise((resolve, reject) => {
        newOrderSchema.findOne({ _id: id }, (error: any, ads: any) => {
          if (error) {
            console.log(error);
            reject({
              success: false,
              message: "There is a problem with your request",
              status: 404,
              data: [],
            });
          } else {
            resolve({
              success: true,
              message: "Successful operation",
              status: 200,
              data: ads,
            });
          }
        });
      });
    },
  },
  Mutation: {
    crearModificarNewOrden: async (root: any, { input }, { usuarioActual }) => {
      const owner = await userSchema.findOne({ _id: input.data.owner });
      const cars = await AdsSchema.findOne({
        _id: input.data.car,
      }).exec();

      if (!usuarioActual) {
        return new Promise((resolve, reject) => {
          return resolve({
            success: false,
            message: "error-login",
            data: null,
          });
        });
      }
      try {
        let newOrder: any;

        if (!cars) {
          return new Promise((resolve, reject) => {
            return resolve({
              success: false,
              message: "no-car-error",
              data: null,
            });
          });
        }

        if (!input.data || !input.data._id) {
          newOrder = new newOrderSchema(input.data);
        }
        return new Promise((resolve, reject) => {
          newOrder.save((error: any, ordenguardada: IOrders) => {
            if (error) {
              console.log(error);
              return resolve({
                success: false,
                message: "Algo salio mal intentalo de nuevo",
                data: null,
              });
            } else {
              UpdateBusyCar(input.data.car, input.data.busyDays);
              pushNotificationNewOrder(
                owner.OnesignalID,
                "Nueva",
                ordenguardada
              );

              if (cars.instantBooking) {
                setTimeout(() => {
                  pushNotificationNewOrder(
                    owner.OnesignalID,
                    "Confirmada",
                    ordenguardada
                  );
                }, 15000);
              }
              return resolve({
                success: true,
                message: "Pedido realizado con éxito",
                data: ordenguardada,
              });
            }
          });
        });
      } catch (error) {
        return new Promise((resolve, reject) => {
          return reject({
            success: false,
            message: "Algo salio mal intentalo de nuevo",
            data: null,
          });
        });
      }
    },

    NewOrdenProceed: async (root: any, { ordenId, status, IntegerValue }) => {
      return new Promise((resolve, reject) => {
        newOrderSchema.findOneAndUpdate(
          { _id: ordenId },
          {
            status: status,
            IntegerValue: Number(IntegerValue),
            $push: {
              statusProcess: {
                status: status,
                date: new Date(),
              },
            },
          },
          { new: true },
          async (error: any, order: IOrders) => {
            if (error) {
              reject({
                messages: "Hay un problema con su solicitud",
                status: 400,
                success: false,
              });
            } else {
              const user = await userSchema.findOne({ _id: order.customer });
              pushNotificationOrderProccess(user.OnesignalID, status, order);
              resolve({
                messages: "Orden procesada con éxito",
                status: 200,
                success: true,
              });
            }
          }
        );
      });
    },
  },

  Order: {
    Owner(parent: any) {
      return new Promise((resolve, reject) => {
        userSchema.findOne({ _id: parent.owner }, (error: any, user: IUser) => {
          if (error) {
            reject(null);
          } else {
            resolve(user);
          }
        });
      });
    },

    Customer(parent: any) {
      return new Promise((resolve, reject) => {
        userSchema.findOne(
          { _id: parent.customer },
          (error: any, user: IUser) => {
            if (error) {
              reject(null);
            } else {
              resolve(user);
            }
          }
        );
      });
    },
    Car(parent: any) {
      return new Promise((resolve, reject) => {
        AdsSchema.findOne({ _id: parent.car }, (error: any, ads: IAds) => {
          if (error) {
            reject(null);
          } else {
            resolve(ads);
          }
        });
      });
    },
  },
};
