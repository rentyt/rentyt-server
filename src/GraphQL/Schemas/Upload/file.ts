import { gql } from "apollo-server-express";
import AWS from "aws-sdk";

AWS.config.update({
  region: process.env.region,
  accessKeyId: process.env.accessKeyId,
  secretAccessKey: process.env.secretAccessKey,
});

var s3Bucket = new AWS.S3({ params: { Bucket: "rentyt" } });

export const typeDefs = gql`
  type FileAWS {
    data: JSON
  }

  extend type Mutation {
    singleUploadToAws(file: Upload): FileAWS
  }
`;

export const resolvers = {
  Mutation: {
    singleUploadToAws(parent: any, { file }) {
      const buf = Buffer.from(
        file.replace(/^data:image\/\w+;base64,/, ""),
        "base64"
      );
      const matches = file.match(/^data:.+\/(.+);base64,(.*)$/);
      const ext = matches[1];
      var data = {
        Key: `${Date.now()}-images-rentyt.${ext}`,
        Body: buf,
        ContentEncoding: "base64",
        ContentType: "image/jpeg",
        ACL: "public-read",
      };
      return new Promise((resolve, reject) => {
        //@ts-ignore
        s3Bucket.upload(data, function (err, data) {
          if (err) {
            console.log(err);
            reject({
              data: null,
            });
          } else {
            resolve({
              data: data,
            });
          }
        });
      });
    },
  },
};
