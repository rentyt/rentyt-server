import { gql } from "apollo-server-express";

// Models
import AdsSchema, { IAds } from "../../../models/ads";
import favouritesSchema, { IFavourites } from "../../../models/favourites";
import lastViewSchema, { ILastView } from "../../../models/lastView";
import userSchema, { IUser } from "../../../models/user";
import { pushNotificationNewCar } from "../../../Funtions/NotificationNewCar";
import { updateCarPosition } from "../../../Funtions/AdsUpdate";

export const typeDefs = gql`
  type CreateAdsResponse {
    success: Boolean!
    message: String!
    data: Ads
  }

  type AdsAllResponse {
    success: Boolean!
    message: String!
    status: Int!
    count: Int
    data: [Ads]
  }

  type AdsResponse {
    success: Boolean!
    message: String!
    status: Int!
    data: Ads
  }

  type AdsFavouritesResponse {
    success: Boolean!
    status: Int!
    message: String!
    data: [AdsFavourites]
  }

  type AdsFavourites {
    _id: ID
    adsID: ID
    userID: ID
    created_at: Date
    updated_at: Date
    Ads: Ads
  }

  type Ads {
    _id: ID
    car: JSON
    instantBooking: Boolean
    inService: Boolean
    minDays: Int
    gestionCost: Int
    anticipation: JSON
    offerts: JSON
    owner: ID
    OwnerData: Usuario
    images: JSON
    description: String
    location: JSON
    details: JSON
    rating: String
    equipemnet: JSON
    ExtraEquipment: JSON
    babyChairs: JSON
    preferences: JSON
    city: String
    prices: JSON
    availability: JSON
    busyDays: JSON
    created_at: Date
    updated_at: Date
    popular: Boolean
    visible: Boolean
    inFavourites: Boolean
    status: String
    statusProcess: JSON
    offertDay: Boolean
    offertMon: Boolean
    airPortPickup: JSON
    airPorts: JSON
    message: String
    display_id: String
    source: JSON
    dist: JSON
  }

  input AdsInput {
    data: JSON
  }

  extend type Query {
    getAds(input: JSON, page: Int, limit: Int, position: JSON): AdsAllResponse
    getAdsOwner(input: JSON): AdsAllResponse
    getAdsForID(id: ID): AdsResponse
    getAdsFavourites(id: ID): AdsFavouritesResponse
    getAdsLastView(id: ID): AdsFavouritesResponse
    getAdsFeed(input: JSON, page: Int, limit: Int): AdsAllResponse
    getAdsTest(coordinates: JSON): AdsAllResponse
  }

  extend type Mutation {
    createdAds(input: AdsInput): CreateAdsResponse
    createFavoritoAds(adsID: ID, userID: ID): generarResponse
    deletedFavourite(id: ID): generarResponse
    createLAstViewAds(adsID: ID, userID: ID): generarResponse
    deletedAds(id: ID!): generarResponse
    updateAds(input: AdsInput): generarResponse
  }
`;

export const resolvers = {
  Mutation: {
    createdAds: async (root: any, { input }) => {
      const newAds = new AdsSchema(input.data);
      return new Promise((resolve, object) => {
        newAds.save(async (error, ads: IAds) => {
          if (error)
            object({
              success: false,
              message: "error_created_ads",
              data: null,
            });
          else {
            const user = await userSchema.findOne({ _id: ads.owner });
            pushNotificationNewCar(
              user.OnesignalID,
              ads.status.replace(/ /g, ""),
              ads
            );
            updateCarPosition(ads);
            resolve({
              success: true,
              message: "success_created_ads",
              data: ads,
            });
          }
        });
      });
    },

    deletedAds: async (root: any, { id }) => {
      return new Promise((resolve, object) => {
        AdsSchema.findOneAndUpdate(
          { _id: id },
          {
            status: "Deleted",
            $push: {
              statusProcess: { status: "Deleted", date: new Date() },
            },
          },
          { upsert: true },
          (error) => {
            if (error) {
              object({
                messages: "Algo salio mal intentalo de nuevo",
                success: false,
                status: 401,
              });
            } else {
              resolve({
                messages: "Anuncio eliminado con éxito",
                success: true,
                status: 200,
              });
            }
          }
        );
      });
    },

    updateAds: async (root: any, { input }) => {
      return new Promise(async (resolve, object) => {
        AdsSchema.findOneAndUpdate(
          { _id: input.data._id },
          input.data,
          { new: true },
          async (error: any, ads: IAds) => {
            if (error) {
              object({
                messages: "Algo salio mal intentalo de nuevo",
                success: false,
                status: 401,
              });
            } else {
              const user = await userSchema.findOne({ _id: ads.owner });
              if (
                (ads.visible && ads.status === "Succees") ||
                ads.status === "Rejected" ||
                ads.status === "In review"
              ) {
                pushNotificationNewCar(
                  user.OnesignalID,
                  ads.status.replace(/ /g, ""),
                  ads
                );
              }
              updateCarPosition(ads);
              resolve({
                messages: "Anuncio actualizado con éxito",
                success: true,
                status: 200,
              });
            }
          }
        );
      });
    },

    createFavoritoAds: async (root: any, { adsID, userID }) => {
      const newFavourite = new favouritesSchema({
        adsID,
        userID,
      });
      return new Promise((resolve, reject) => {
        newFavourite.save((error: any) => {
          if (error) {
            reject({
              messages: "add_favourite_error",
              success: false,
              status: 404,
            });
          } else {
            resolve({
              messages: "Add_favourite_success",
              success: true,
              status: 200,
            });
          }
        });
      });
    },

    deletedFavourite: async (root: any, { id }) => {
      return new Promise((resolve, reject) => {
        favouritesSchema.findOneAndDelete(
          { adsID: id },
          //@ts-ignore
          (error: any) => {
            if (error)
              reject({
                messages: "deteted_favourite_error",
                success: false,
                status: 404,
              });
            else
              resolve({
                messages: "delete_favourite_success",
                success: true,
                status: 200,
              });
          }
        );
      });
    },

    createLAstViewAds: async (root: any, { adsID, userID }) => {
      const isExist = await lastViewSchema.findOne({
        adsID: adsID,
        userID: userID,
      });
      return new Promise((resolve, reject) => {
        if (isExist) {
          resolve({
            messages: "You have already visited this ad",
            success: true,
            status: 200,
          });
        } else {
          const newLastView = new lastViewSchema({
            adsID,
            userID,
          });

          newLastView.save((error: any) => {
            if (error) {
              reject({
                messages: "add_favourite_error",
                success: false,
                status: 404,
              });
            } else {
              resolve({
                messages: "Add_favourite_success",
                success: true,
                status: 200,
              });
            }
          });
        }
      });
    },
  },

  Query: {
    getAdsFeed: (root: any, { input, page, limit }) => {
      console.log(page, limit);
      return new Promise((resolve, reject) => {
        let condition = {};

        //@ts-ignore
        if (input.status) condition = { status: input.status, visible: true };

        //@ts-ignore
        if (input.marke) condition = { "car.marker": input.marke };

        //@ts-ignore
        if (input.marke) condition = { "car.marker": input.marke };

        //@ts-ignore
        if (input.year.length > 0)
          //@ts-ignore
          condition = { "car.year": { $in: input.year } };

        //@ts-ignore
        if (input.models.length > 0)
          //@ts-ignore
          condition = {
            "car.model": {
              $in: input.models,
            },
          };

        //@ts-ignore
        if (input.instantBooking)
          //@ts-ignore
          condition = {
            instantBooking: input.instantBooking,
          };

        //@ts-ignore
        if (input.popular)
          //@ts-ignore
          condition = { popular: input.popular };

        //@ts-ignore
        if (input.offert)
          //@ts-ignore
          condition = { offertDay: input.offert };

        if (input.userCategory.length > 0)
          //@ts-ignore
          condition = {
            "car.marker": { $in: input.userCategory },
          };

        if (input.airPortPickup)
          //@ts-ignore
          condition = {
            "airPortPickup.available": input.airPortPickup,
          };

        if (input.price > 1000) {
          // @ts-ignore
          condition = {
            "prices.value": {
              $gte: 1000,
              $lt: input.price,
            },
          };
        }

        if (input.minimeDay > 1) {
          // @ts-ignore
          condition = {
            minDays: {
              $gte: 1,
              $lt: input.minimeDay,
            },
          };
        }

        AdsSchema.aggregate([
          {
            $geoNear: {
              near: {
                type: "Point",
                coordinates: [input.position.lat, input.position.lgn],
              },
              distanceField: "dist.calculated",
              includeLocs: "dist.location",
              query: condition,
              spherical: true,
            },
          },
          { $skip: (page - 1) * limit },
          { $limit: limit * 1 },
        ])
          .then((res) => {
            resolve({
              success: true,
              message: "Successful operation",
              status: 200,
              data: res,
            });
          })
          .catch((err) => {
            console.error(err);
            reject({
              success: false,
              message: "There is a problem with your request",
              status: 404,
              data: [],
            });
          });
      });
    },

    getAds: (root: any, { input, page, limit, position }) => {
      return new Promise((resolve, reject) => {
        AdsSchema.aggregate([
          {
            $geoNear: {
              near: {
                type: "Point",
                coordinates: position,
              },
              distanceField: "dist.calculated",
              includeLocs: "dist.location",
              query: input,
              spherical: true,
            },
          },
          { $skip: (page - 1) * limit },
          { $limit: limit * 1 },
        ])
          .then((res) => {
            resolve({
              success: true,
              message: "Successful operation",
              status: 200,
              data: res,
            });
          })
          .catch((err) => {
            reject({
              success: false,
              message: "There is a problem with your request",
              status: 404,
              data: [],
            });
          });
      });
    },

    getAdsForID: (root: any, { id }) => {
      return new Promise((resolve, reject) => {
        AdsSchema.findOne({ _id: id }, (error: any, ads: any) => {
          if (error) {
            reject({
              success: false,
              message: "There is a problem with your request",
              status: 404,
              data: [],
            });
          } else {
            resolve({
              success: true,
              message: "Successful operation",
              status: 200,
              data: ads,
            });
          }
        });
      });
    },

    getAdsOwner: (root: any, { input }) => {
      return new Promise((resolve, reject) => {
        AdsSchema.find(input, (error: any, ads: any) => {
          if (error) {
            reject({
              success: false,
              message: "There is a problem with your request",
              status: 404,
              data: [],
            });
          } else {
            resolve({
              success: true,
              message: "Successful operation",
              status: 200,
              data: ads,
            });
          }
        });
      });
    },

    getAdsFavourites: (root: any, { id }) => {
      return new Promise((resolve, reject) => {
        favouritesSchema
          .find({ userID: id }, (error: any, favourite: IFavourites) => {
            if (error) {
              reject({
                success: false,
                status: 404,
                message: "There is a problem with your request",
                data: [],
              });
            } else {
              resolve({
                success: true,
                status: 200,
                message: "Successful operation",
                data: favourite,
              });
            }
          })
          .sort({ $natural: -1 });
      });
    },

    getAdsLastView: (root: any, { id }) => {
      return new Promise((resolve, reject) => {
        lastViewSchema
          .find({ userID: id }, (error: any, lastView: ILastView) => {
            if (error) {
              reject({
                success: false,
                status: 404,
                message: "There is a problem with your request",
                data: [],
              });
            } else {
              resolve({
                success: true,
                status: 200,
                message: "Successful operation",
                data: lastView,
              });
            }
          })
          .limit(10)
          .sort({ $natural: -1 });
      });
    },

    getAdsTest: (root: any, { coordinates }) => {
      return new Promise((resolve, reject) => {
        AdsSchema.aggregate([
          {
            $geoNear: {
              near: { type: "Point", coordinates: coordinates },
              maxDistance: 100000,
              distanceField: "dist.calculated",
              includeLocs: "dist.location",
              spherical: true,
            },
          },
        ])
          .then((res) => {
            resolve({
              message: "Successful operation",
              success: true,
              status: 200,
              data: res,
            });
          })
          .catch((err) => {
            console.error(err);
            reject({
              success: false,
              status: 404,
              message: "There is a problem with your request",
              data: err,
            });
          });
      });
    },
  },

  Ads: {
    OwnerData(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.owner }, (error: any, user: IUser) => {
          if (error) {
            rejects(null);
          } else {
            resolve(user);
          }
        });
      });
    },

    inFavourites(parent: any, args: any, { usuarioActual }) {
      return new Promise((resolve, reject) => {
        if (!usuarioActual) resolve(false);
        userSchema.findOne(
          { _id: usuarioActual._id },
          (error: any, usuario: IUser) => {
            if (error) reject(error);
            else {
              if (!usuario) resolve(false);
              else {
                favouritesSchema.findOne(
                  { userID: usuario._id, adsID: parent._id },
                  (error, favorito) => {
                    if (error) reject(error);
                    else {
                      if (!favorito) resolve(false);
                      else resolve(true);
                    }
                  }
                );
              }
            }
          }
        );
      });
    },
  },

  AdsFavourites: {
    Ads(parent: any) {
      return new Promise((resolve, reject) => {
        AdsSchema.findOne(
          { _id: parent.adsID, visible: true, status: "Succees" },
          (error: any, ads: IAds) => {
            if (error) {
              reject(null);
            } else {
              resolve(ads);
            }
          }
        );
      });
    },
  },
};
