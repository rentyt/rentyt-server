import { typeDefs, resolvers } from "./Resolvers/Schema";
import { ApolloServer } from "apollo-server-express";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });

const GQservers = new ApolloServer({
  typeDefs,
  resolvers,
  playground: true,
  introspection: true,
  context: async ({ req }) => {
    const token = req.headers["authorization"];
    if (token !== null) {
      try {
        const usuarioActual = jwt.verify(
          JSON.parse(token),
          process.env.SECRETO
        );
        req.usuarioActual = usuarioActual;
        return {
          usuarioActual,
        };
      } catch (err) {
        //console.log("err: ", err);
      }
    }
  },
});

export default GQservers;
