import { gql } from "apollo-server-express";
import { typeDefs as user } from "../Schemas/Users/user";
import { resolvers as userResolvers } from "../Schemas/Users/user";
import { resolvers as carResolvers } from "../Schemas/Cars/cars";
import { typeDefs as cars } from "../Schemas/Cars/cars";
import { typeDefs as File } from "../Schemas/Upload/file";
import { resolvers as FileResolver } from "../Schemas/Upload/file";
import { typeDefs as order } from "../Schemas/Order/order";
import { resolvers as orderResolvers } from "../Schemas/Order/order";
import { resolvers as cuponResolvers } from "../Schemas/Cupon";
import { typeDefs as cupon } from "../Schemas/Cupon";
import { typeDefs as rating } from "../Schemas/Rating/rating";
import { resolvers as ratingResolver } from "../Schemas/Rating/rating";
import { typeDefs as adminUser } from "../Dashboard/Users/user";
import { resolvers as adminUserResolver } from "../Dashboard/Users/user";
import { typeDefs as adminCar } from "../Dashboard/Cars/cars";
import { resolvers as adminCarResolver } from "../Dashboard/Cars/cars";
import { typeDefs as adminReserva } from "../Dashboard/Order/order";
import { resolvers as adminReservaResolver } from "../Dashboard/Order/order";
import { typeDefs as adminPost } from "../Dashboard/BlogPost/post";
import { resolvers as adminPostResolver } from "../Dashboard/BlogPost/post";
import { typeDefs as blogPost } from "../Schemas/BlogPost/post";
import { resolvers as PostResolver } from "../Schemas/BlogPost/post";
import { typeDefs as phone } from "../Schemas/Phone/phone";
import { resolvers as phoneResolver } from "../Schemas/Phone/phone";

const rootTypeDefs = gql`
  scalar Date
  scalar JSON
  scalar JSONObject

  type generarResponse {
    messages: String!
    success: Boolean!
    status: Int!
  }

  type Query {
    _: String
  }

  type Mutation {
    _: String
  }
`;

export const typeDefs = [
  rootTypeDefs,
  user,
  cars,
  File,
  order,
  cupon,
  rating,
  adminUser,
  adminCar,
  adminReserva,
  adminPost,
  blogPost,
  phone,
];
export const resolvers = [
  userResolvers,
  carResolvers,
  FileResolver,
  orderResolvers,
  cuponResolvers,
  ratingResolver,
  adminUserResolver,
  adminCarResolver,
  adminReservaResolver,
  adminPostResolver,
  PostResolver,
  phoneResolver,
];
