import { gql } from "apollo-server-express";
import newOrderSchema from "../../../models/order";

export const typeDefs = gql`
  extend type Query {
    getAllOrderAdmin(id: Int, page: Int, limit: Int): newOrderAllResponse
  }
`;

export const resolvers = {
  Query: {
    getAllOrderAdmin: async (root: any, { id, page, limit }) => {
      var value = await newOrderSchema.countDocuments();

      return new Promise((resolve, reject) => {
        let condition = {};
        //@ts-ignore
        if (id > 0) condition.channelOrderDisplayId = id;

        newOrderSchema
          .find(condition, (error: any, order: any) => {
            if (error) {
              reject({
                success: false,
                message: "There is a problem with your request",
                status: 404,
                count: 0,
                data: [],
              });
            } else {
              resolve({
                success: true,
                message: "Successful operation",
                status: 200,
                count: value,
                data: order,
              });
            }
          })
          .sort({ $natural: -1 })
          .limit(limit * 1)
          .skip((page - 1) * limit)
          .exec();
      });
    },
  },
};
