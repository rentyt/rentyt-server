import bcrypt from "bcryptjs";
import { crearToken } from "../../createToken";
import userSchema from "../../../models/user";
import { gql } from "apollo-server-express";
import { validateEmail } from "./validateEmail";

export const typeDefs = gql`
  type userResponseAdmin {
    success: Boolean!
    message: String
    status: Int
    count: Int
    data: [Usuario]
  }

  extend type Query {
    getUserAdmin(search: String, page: Int, limit: Int): userResponseAdmin
  }

  extend type Mutation {
    LoginAdminUser(email: String, password: String): OauthUserResponse
  }
`;

export const resolvers = {
  Mutation: {
    LoginAdminUser: async (root: any, { email, password }) => {
      const users = await userSchema.findOne({ email });
      if (!users) {
        return {
          success: false,
          message: "Aún no de te has registrado en Rentyt",
          data: null,
        };
      } else {
        if (users.isAdmin) {
          const passwordCorrecto = await bcrypt.compare(
            password,
            users.password
          );

          if (users.isAvalible) {
            if (!passwordCorrecto) {
              return {
                success: false,
                message: "Contraseña incorrecta",
                data: null,
              };
            } else {
              return {
                success: true,
                message: "Bienvenido a Rentyt Team",
                data: {
                  token: crearToken(users, process.env.SECRETO, "4h"),
                  id: users._id,
                  user: users,
                  verifyPhone: users.verifyPhone,
                },
              };
            }
          } else {
            return {
              success: false,
              message: "Tu cuenta no es dispinible o ha sido suspendida",
              data: null,
            };
          }
        } else {
          return {
            success: false,
            message: "No tiene suficiente permiso para acceder",
            data: null,
          };
        }
      }
    },
  },

  Query: {
    getUserAdmin: async (root: any, { search, page, limit }) => {
      var value = await userSchema.countDocuments();
      return new Promise((resolve, reject) => {
        let condition = {};

        //@ts-ignore
        if (search)
          if (validateEmail(search)) {
            //@ts-ignore
            condition.email = search;
          } else {
            condition = {
              $text: { $search: `"\"${search} \""` },
            };
          }

        userSchema
          .find(condition, (error: any, user: any) => {
            if (error) {
              reject({
                success: false,
                message: "There is a problem with your request",
                status: 404,
                count: 0,
                data: null,
              });
            } else {
              resolve({
                success: true,
                message: "Successful operation",
                status: 200,
                count: value,
                data: user,
              });
            }
          })
          .sort(search ? null : { $natural: -1 })
          .limit(limit * 1)
          .skip((page - 1) * limit)
          .exec();
      });
    },
  },
};
