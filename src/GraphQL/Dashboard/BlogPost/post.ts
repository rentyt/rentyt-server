import { gql } from "apollo-server-express";
// Models
import postSchema from "../../../models/post";

export const typeDefs = gql`
  extend type Mutation {
    crearPost(input: JSON): createdPostResponse
  }
`;

export const resolvers = {
  Mutation: {
    crearPost: async (root: any, { input }) => {
      const nuevoPost = new postSchema(input);
      return new Promise((resolve, object) => {
        nuevoPost.save(async (error) => {
          if (error)
            object({
              success: false,
              message: "Hubo un problema con su solicitud",
            });
          else {
            resolve({
              success: true,
              message: "Post publicado con éxito",
            });
          }
        });
      });
    },
  },
};
