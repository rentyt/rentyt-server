import { gql } from "apollo-server-express";
// Models
import AdsSchema from "../../../models/ads";

export const typeDefs = gql`
  extend type Query {
    getAdsForIDAdmin(id: ID): AdsResponse
    getAdsAdmin(id: String, page: Int, limit: Int): AdsAllResponse
  }
`;

export const resolvers = {
  Query: {
    getAdsAdmin: async (root: any, { id, page, limit }) => {
      var value = await AdsSchema.countDocuments();

      return new Promise((resolve, reject) => {
        let condition = {};

        //@ts-ignore
        if (id) condition.display_id = id;

        AdsSchema.find(condition, (error: any, ads: any) => {
          if (error) {
            reject({
              success: false,
              message: "There is a problem with your request",
              status: 404,
              count: 0,
              data: [],
            });
          } else {
            resolve({
              success: true,
              message: "Successful operation",
              status: 200,
              count: value,
              data: ads,
            });
          }
        })
          .sort({ $natural: -1 })
          .limit(limit * 1)
          .skip((page - 1) * limit)
          .exec();
      });
    },

    getAdsForIDAdmin: (root: any, { id }) => {
      return new Promise((resolve, reject) => {
        AdsSchema.findOne({ _id: id }, (error: any, ads: any) => {
          if (error) {
            reject({
              success: false,
              message: "There is a problem with your request",
              status: 404,
              data: [],
            });
          } else {
            resolve({
              success: true,
              message: "Successful operation",
              status: 200,
              data: ads,
            });
          }
        });
      });
    },
  },
};
