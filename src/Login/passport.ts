"use strict";
import {
  FACEBOOK_APP_ID,
  GOOGLE_CLIENT_ID,
  google_customer_secret,
  FACEBOOK_SECRET,
} from "./config";
import bcrypt from "bcryptjs";
import userSchema from "../models/user";
import { createStripeClient } from "../libs/CreateStripeClient";
import { SaveEmailToMailJet } from "../Mail/createContact";
import { welcomeEmail } from "../Emails/Welcome/sendEmail";
var passport = require("passport");
var FacebookTokenStrategy = require("passport-facebook-token");
var GoogleTokenStrategy = require("passport-google-token").Strategy;

module.exports = function () {
  passport.use(
    new FacebookTokenStrategy(
      {
        clientID: FACEBOOK_APP_ID,
        clientSecret: FACEBOOK_SECRET,
      },
      async (accessToken: any, refreshToken: any, profile: any, done: any) => {
        require("mongoose")
          .model("user")
          .schema.add({ isSocial: Boolean, socialNetworks: String });
        let emailExists = await userSchema.findOne({
          email: profile.emails[0].value,
        });
        if (emailExists) {
          bcrypt.genSalt(10, (err, salt) => {
            if (err) {
              console.log(err);
            } else {
              bcrypt.hash(accessToken, salt, (err, hash) => {
                if (err) {
                  console.log(err);
                } else {
                  userSchema.findOneAndUpdate(
                    { email: profile.emails[0].value },
                    { password: hash },
                    { new: true },
                    (err: any, updated: any) => {
                      if (err) {
                        console.log(err);
                      } else {
                        return done(err, {
                          nuevoUsuario: updated,
                          token: accessToken,
                        });
                      }
                    }
                  );
                }
              });
            }
          });
        } else {
          const nuevoUsuario = new userSchema({
            id: profile.id,
            password: accessToken,
            email: profile.emails[0].value,
            name: profile.name.givenName,
            lastName: profile.name.familyName,
            isSocial: true,
            socialNetworks: "Facebook",
          });
          nuevoUsuario.id = nuevoUsuario._id;

          createStripeClient(
            nuevoUsuario.name,
            nuevoUsuario.email,
            nuevoUsuario._id
          );
          SaveEmailToMailJet(
            nuevoUsuario.email,
            nuevoUsuario.name,
            nuevoUsuario.lastName
          );

          welcomeEmail(nuevoUsuario.email);

          nuevoUsuario.save((error) => {
            return done(error, { nuevoUsuario, token: accessToken });
          });
        }
      }
    )
  );

  passport.use(
    new GoogleTokenStrategy(
      {
        clientID: GOOGLE_CLIENT_ID,
        clientSecret: google_customer_secret,
      },
      async (accessToken: any, refreshToken: any, profile: any, done: any) => {
        require("mongoose")
          .model("user")
          .schema.add({ isSocial: Boolean, socialNetworks: String });
        // check if email exists
        let emailExists = await userSchema.findOne({
          email: profile.emails[0].value,
        });

        if (emailExists) {
          bcrypt.genSalt(10, (err, salt) => {
            if (err) {
              console.log(err);
            } else {
              bcrypt.hash(accessToken, salt, (err, hash) => {
                if (err) {
                  console.log(err);
                } else {
                  userSchema.findOneAndUpdate(
                    { email: profile.emails[0].value },
                    { password: hash },
                    { new: true },
                    (err: any, updated: any) => {
                      if (err) {
                        console.log(err);
                      } else {
                        let nuevoUsuario = updated;
                        return done(err, { nuevoUsuario, token: accessToken });
                      }
                    }
                  );
                }
              });
            }
          });
        } else {
          const nuevoUsuario = new userSchema({
            password: accessToken,
            email: profile.emails[0].value,
            name: profile.name.givenName,
            lastName: profile.name.familyName,
            isSocial: true,
            socialNetworks: "Google",
          });
          nuevoUsuario.id = nuevoUsuario._id;

          createStripeClient(
            nuevoUsuario.name,
            nuevoUsuario.email,
            nuevoUsuario._id
          );
          SaveEmailToMailJet(
            nuevoUsuario.email,
            nuevoUsuario.name,
            nuevoUsuario.lastName
          );

          welcomeEmail(nuevoUsuario.email);

          nuevoUsuario.save((error) => {
            return done(error, { nuevoUsuario, token: accessToken });
          });
        }
      }
    )
  );
};
