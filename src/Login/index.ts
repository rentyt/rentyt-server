import { Router } from "express";
import userSchema, { IUser } from "../models/user";
import bcrypt from "bcrypt";
import crypto from "crypto";
import appleSigninAuth from "apple-signin-auth";
import { createStripeClient } from "../libs/CreateStripeClient";
import { SaveEmailToMailJet } from "../Mail/createContact";
import { welcomeEmail } from "../Emails/Welcome/sendEmail";
import jwt from "jsonwebtoken";
var passport = require("passport");
require("./passport")();

var createToken = function (auth: any) {
  return jwt.sign(
    {
      id: auth.id,
    },
    process.env.SECRETO || "secretToken",
    {
      expiresIn: "9999 years",
    }
  );
};

function generateToken(req: any, res: any, next: any) {
  req.token = createToken(req.auth);
  return next();
}

function sendToken(req: any, res: any) {
  res.setHeader("x-auth-token", req.token);
  return res.status(200).send(JSON.stringify(req.user));
}

const routerLogin = Router();

routerLogin.post("/api/v1/auth/social/mobile", async function (req, res) {
  require("mongoose").model("user").schema.add({
    appleToken: String,
    socialNetworks: String,
    isSocial: Boolean,
  });
  let data = req.body;
  const tokens = data.token;
  let emailExistsApple = await userSchema.findOne({
    appleToken: data.token,
  });

  if (emailExistsApple) {
    const nuevoUsuario = emailExistsApple;
    res.json({ nuevoUsuario, token: tokens });
  } else {
    let emailExists = await userSchema.findOne({
      email: data.email,
    });

    if (emailExists) {
      bcrypt.genSalt(10, (err: any, salt: any) => {
        if (err) console.log(err);
        bcrypt.hash(data.token, salt, (err: any, hash: any) => {
          if (err) console.log(err);
          userSchema.findOneAndUpdate(
            { email: data.email },
            { password: hash },
            //@ts-ignore
            (err: any, user: IUser) => {
              if (err) {
                res.json(err);
              }
              let nuevoUsuario = user;
              res.json({ nuevoUsuario, token: tokens });
            }
          );
        });
      });
    } else {
      const nuevoUsuarios = new userSchema({
        name: data.firstName,
        lastName: data.lastName,
        email: data.email,
        password: data.token,
        isSocial: true,
        city: data.city,
        appleToken: data.token,
        socialNetworks: data.provide,
        lastSeen: new Date(),
      });

      nuevoUsuarios.save(async (error: any) => {
        if (error) {
          return res.json(error);
        } else {
          createStripeClient(
            nuevoUsuarios.name,
            nuevoUsuarios.email,
            nuevoUsuarios._id
          );
          SaveEmailToMailJet(
            nuevoUsuarios.email,
            nuevoUsuarios.name,
            nuevoUsuarios.lastName
          );

          welcomeEmail(nuevoUsuarios.email);

          const nuevoUsuario = nuevoUsuarios;
          return res.json({ nuevoUsuario, token: tokens });
        }
      });
    }
  }
});

routerLogin.post(
  "/api/v1/auth/facebook",
  passport.authenticate("facebook-token", { session: false }),
  function (req: any, res: any, next: any) {
    if (!req.user) {
      return res.send(401);
    }
    req.auth = {
      id: req.user.id,
    };

    next();
  },
  generateToken,
  sendToken
);

routerLogin.post(
  "/api/auth/apple-web",
  async function (req: any, res: any, next: any) {
    require("mongoose").model("user").schema.add({
      appleToken: String,
      socialNetworks: String,
      isSocial: Boolean,
    });
    const { id_token, user } = req.body;
    const appleIdTokenClaims = await appleSigninAuth.verifyIdToken(id_token, {
      nonce: "nonce"
        ? crypto.createHash("sha256").update("nonce").digest("hex")
        : undefined,
    });

    // // check if email exists
    let emailExistsApple = await userSchema.findOne({
      appleToken: appleIdTokenClaims.sub,
    });

    if (emailExistsApple) {
      const nuevoUsuario = emailExistsApple;
      res.json({ nuevoUsuario, token: appleIdTokenClaims.sub });
    } else {
      const nuevoUsuario = new userSchema({
        name: user.name.firstName,
        lastName: user.name.lastName,
        email: user.email,
        password: appleIdTokenClaims.sub,
        isSocial: true,
        appleToken: appleIdTokenClaims.sub,
        socialNetworks: "Apple",
      });

      nuevoUsuario.id = nuevoUsuario._id;

      nuevoUsuario.save(async (error: any) => {
        if (error) {
          return res.json(error);
        } else {
          createStripeClient(
            nuevoUsuario.name,
            nuevoUsuario.email,
            nuevoUsuario._id
          );
          SaveEmailToMailJet(
            nuevoUsuario.email,
            nuevoUsuario.name,
            nuevoUsuario.lastName
          );

          welcomeEmail(nuevoUsuario.email);

          return res.json({ nuevoUsuario, token: appleIdTokenClaims.sub });
        }
      });
    }
  }
);

routerLogin.post(
  "/api/v1/auth/google",
  passport.authenticate("google-token", { session: false }),
  function (req: any, res: any, next: any) {
    if (!req.user) {
      return res.send(401);
    }
    req.auth = {
      id: req.user.id,
    };

    next();
  },
  generateToken,
  sendToken
);

export default routerLogin;
