import { Router, Request, Response } from "express";
import userSchema from "../../models/user";
import { sendNotifications } from "../Notification/Notification";
import { titleData } from "./languaTitles";
import { messageLang } from "./languajeMessages";

var request = require("request");

const routerVerify = Router();

routerVerify.post("/sessions", (req: Request, res: Response) => {
  //@ts-ignore
  const { user, location, city, additionalAddress } = req.body;

  if (user) {
    userSchema.findOneAndUpdate(
      { _id: user._id },
      {
        location: location,
        city: city,
        additionalAddress: additionalAddress,
      },
      { new: true },
      (err, user) => {}
    );
  }

  var options = {
    method: "POST",
    url: "https://stationapi.veriff.com/v1/sessions/",
    headers: {
      "Content-Type": "application/json",
      "X-AUTH-CLIENT": "59d2b46f-1a13-47ba-b2c0-71630d4cc040",
    },
    body: {
      verification: {
        callback: "https://rentytapp.com",
        vendorData: user.email,
        timestamp: new Date(),
      },
    },
    json: true,
  };

  request(options, function (error, response, body) {
    if (error) {
      res
        .status(400)
        .json({ success: false, message: "Error del servidor", data: [] })
        .end();
    } else {
      res
        .status(200)
        .json({ success: true, message: "url creada con exito", data: body })
        .end();
    }
  });
});

routerVerify.post("/hook/veriff", (req: Request, res: Response) => {
  res
    .status(200)
    .json({
      success: true,
      message: "Proceso exitoso",
      //@ts-ignore
      data: req.body,
    })
    .end();
});

routerVerify.post("/hook/success/veriff", (req: Request, res: Response) => {
  //@ts-ignore
  const { status, verification, technicalData } = req.body;
  if (status === "success") {
    userSchema.findOneAndUpdate(
      { email: verification.vendorData },
      {
        verified: true,
        status: status,
        verification: verification,
        technicalData: technicalData,
      },
      { new: true },
      (err, user) => {
        if (err) {
          res
            .status(400)
            .json({
              success: false,
              message: "Error server",
              data: null,
            })
            .end();
        } else {
          sendNotifications(
            user.OnesignalID,
            messageLang().success.es,
            messageLang().success.en,
            titleData().success.es,
            titleData().success.en,
            user.avatar
          );
          res
            .status(200)
            .json({
              success: true,
              message: "url creada con exito",
              data: user,
            })
            .end();
        }
      }
    );
  } else {
    res
      .status(200)
      .json({ success: false, message: "Error server", data: null })
      .end();
  }
});

export default routerVerify;
