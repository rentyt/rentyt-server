export const messageLang = () => {
  return {
    success: {
      en: "Your account has been verified and you are ready to start renting vehicles on Rentyt",
      es: "Tu cuenta ha sido verificada y ya estas listo para empezar a rentar vehículos en Rentyt",
    },
  };
};
