export const titleData = () => {
  return {
    success: {
      en: "Your account has been verified",
      es: "Tu cuenta ha sido verificada",
    },
  };
};
