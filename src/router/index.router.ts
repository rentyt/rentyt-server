import { Router, Request, Response } from "express";
import reportSchema from "../models/report";

const router = Router();

router.get("/", (req: Request, res: Response) => {
  res.redirect("https://rentytapp.com");
});

router.post("/created-report", async (req: Request, res: Response) => {
  //@ts-ignore
  const data = req.body;
  const newReport = new reportSchema(data);
  newReport.save((error: any) => {
    if (error) {
      res.status(200).json({ status: "error" }).end();
    } else {
      res.status(200).json({ status: "success" }).end();
    }
  });
});

export default router;
