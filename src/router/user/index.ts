import { Router, Request, Response } from "express";
import userSchema, { IUser } from "../../models/user";
import { UpdateUser } from "../../socket/utils";
import dotenv from "dotenv";
import jwt from "jsonwebtoken";
dotenv.config({ path: "variables.env" });
const client = require("@bigdatacloudapi/client")(
  "e04215222f1f473ba5c8d8481de62da3"
);

const routerUser = Router();

routerUser.get("/get-user-auth", (req: Request, res: Response) => {
  const { token } = req.headers;
  if (token !== null) {
    try {
      //@ts-ignore
      const usuarioActual = jwt.verify(token, process.env.SECRETO);

      if (usuarioActual) {
        //@ts-ignore
        userSchema.findOne(
          //@ts-ignore
          { _id: usuarioActual._id },
          (err: any, user: IUser) => {
            if (err) {
              res
                .status(400)
                .json({
                  success: false,
                  message: "Error del servidor",
                  error: err,
                  data: null,
                })
                .end();
            } else {
              UpdateUser(user._id);
              res
                .status(200)
                .json({
                  success: true,
                  message: "Data extraida con éxito",
                  data: user,
                })
                .end();
            }
          }
        );
      } else {
        res
          .status(404)
          .json({
            success: false,
            message: "Debes iniciar sesión para continuar",
            error: null,
            data: null,
          })
          .end();
      }
    } catch (err) {
      if (err.message === "jwt expired") {
        res
          .status(404)
          .json({
            success: false,
            message: "Tu sesión he expirado",
            error: err,
            data: null,
          })
          .end();
      }
    }
  } else {
    res
      .status(404)
      .json({
        success: false,
        message: "Debes iniciar sesión para continuar",
        error: null,
        data: null,
      })
      .end();
  }
});

routerUser.get("/get-userinfo-ip", (req: Request, res: Response) => {
  const { ip } = req.query;
  if (ip) {
    client
      .getIpGeolocationFull({ ip: ip })
      .then((jsonResult) => {
        res.status(200).json(jsonResult).end();
      })
      .catch(function (error) {
        res.status(400).json(error).end();
      });
  } else {
    res.status(300).json({ message: "Debes indicar una dirección IP" }).end();
  }
});

export default routerUser;
