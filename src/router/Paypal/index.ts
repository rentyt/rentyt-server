import { Router, Request, Response } from "express";
import dotenv from "dotenv";
import paypal from "paypal-rest-sdk";
dotenv.config({ path: "variables.env" });

paypal.configure({
  mode: "live", //sandbox or live
  client_id: process.env.PAYPAL_CLIENT_ID || "",
  client_secret: process.env.PAYPAL_CLIENT_SECRET || "",
});

const routerPaypal = Router();

routerPaypal.get("/paypal", (req: Request, res: Response) => {
  const { price, currency } = req.query;
  let create_payment_json = {
    intent: "sale",
    payer: {
      payment_method: "paypal",
    },
    redirect_urls: {
      return_url: `https://api.rentytapp.com/success?price=${price}&currency=${currency}`,
      cancel_url: "https://api.rentytapp.com/cancel",
    },
    transactions: [
      {
        amount: {
          currency: currency ? currency : "USD",
          total: req.query.price,
        },
        description: "Rentyt App",
      },
    ],
  };
  // @ts-ignore
  paypal.payment.create(create_payment_json, function (error, payment) {
    if (error) {
      res.redirect("/cancel");
    } else {
      // @ts-ignore
      res.redirect(payment.links[1].href);
    }
  });
});

routerPaypal.get("/success", (req: Request, res: Response) => {
  var PayerID = req.query.PayerID;
  var paymentId = req.query.paymentId;
  const { price, currency } = req.query;
  var execute_payment_json = {
    payer_id: PayerID,
    transactions: [
      {
        amount: {
          currency: currency ? currency : "USD",
          total: price,
        },
      },
    ],
  };
  // @ts-ignore
  paypal.payment.execute(
    // @ts-ignore
    paymentId,
    execute_payment_json,
    function (error, payment) {
      if (error) {
        res.redirect("/cancel");
      }
      if (payment) {
        res.render("success");
      }
    }
  );
});

routerPaypal.get("/cancel", (req: Request, res: Response) => {
  res.render("cancel");
});

export default routerPaypal;
