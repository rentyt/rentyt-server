export const sendNotifications = (
  IdOnesignal: any,
  textmessage_es: string,
  textmessage_en: string,
  title_es: string,
  title_en: string,
  image?: string,
  data?: any
) => {
  var sendNotification = function (data: any) {
    var headers = {
      "Content-Type": "application/json; charset=utf-8",
    };
    var options = {
      host: "onesignal.com",
      port: 443,
      path: "/api/v1/notifications",
      method: "POST",
      headers: headers,
    };
    var https = require("https");
    var req = https.request(options, function (res: any) {
      res.on("data", function (data: any) {
        console.log(JSON.parse(data));
      });
    });

    req.on("error", function (e: any) {
      console.log("ERROR:");
      console.log(e);
    });
    req.write(JSON.stringify(data));
    req.end();
  };

  var message = {
    app_id: "0bd7fc89-c247-4972-8aeb-cc65af8f7fb0",
    headings: { en: title_en, es: title_es },
    android_channel_id: "e5ca1cca-9de7-4966-a584-64efd15f72da",
    ios_sound: "notification_sound.wav",
    contents: { en: `${textmessage_en}`, es: `${textmessage_es}` },
    ios_badgeCount: 1,
    ios_badgeType: "Increase",
    include_player_ids: [IdOnesignal],
    ios_attachments: image ? { id1: image } : null,
    big_picture: image ? image : null,
    data: data ? data : {},
  };
  sendNotification(message);
};
