import { Router, Request, Response } from "express";
import { sendNotifications } from "./Notification";
import userSchema, { IUser } from "../../models/user";

const routerNotification = Router();

routerNotification.get("/send-notification", (req: Request, res: Response) => {
  //@ts-ignore
  const data = req.body;

  sendNotifications(
    "ac15073c-5c1a-11ec-84bb-be32b8dd1c64",
    "Test Sonido",
    "Test sonido",
    "Test Sonido",
    "Test sonido"
  );

  res.status(200).end();
});

routerNotification.post(
  "/user-notification-id-save",
  (req: Request, res: Response) => {
    //@ts-ignore
    const { OnesignalID, user_id } = req.body;

    if (OnesignalID && user_id) {
      userSchema.findOneAndUpdate(
        { _id: user_id },
        { OnesignalID: OnesignalID },
        //@ts-ignore
        (err: any, user: IUser) => {
          if (err) {
            res.status(400).json({ succeeded: false }).end();
          } else if (user) {
            res.status(200).json({ succeeded: true }).end();
          } else {
            res.status(400).json({ succeeded: false }).end();
          }
        }
      );
    } else {
      res
        .status(400)
        .json({ succeeded: false, message: "No user id or notification id" })
        .end();
    }
  }
);

export default routerNotification;
