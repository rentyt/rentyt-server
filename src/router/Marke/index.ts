import { Router, Request, Response } from "express";
import markeSchema from "../../models/selectMark";

const routerMarke = Router();

routerMarke.get("/get-mark", (req: Request, res: Response) => {
  markeSchema.find((err, mark) => {
    if (err) {
      res
        .status(400)
        .json({ success: false, message: "Error del servidor", data: [] })
        .end();
    } else {
      res
        .status(200)
        .json({ success: true, message: "Data extraida con éxito", data: mark })
        .end();
    }
  });
});

export default routerMarke;
