import { Router, Request, Response } from "express";
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });
const { Translate } = require("@google-cloud/translate").v2;

const CREDENTIAL = JSON.parse(process.env.CREDENTIALS);

const options = {
  credentials: CREDENTIAL,
  projectId: CREDENTIAL.project_id,
};

const translate = new Translate(options);

const routerTranslation = Router();

routerTranslation.post("/translate-text", (req: Request, res: Response) => {
  //@ts-ignore
  const data = req.body;

  const text = data.text;
  const originLanguage = data.target;

  async function translateText() {
    let [detections] = await translate.detect(text);

    const target =
      originLanguage === detections.language
        ? originLanguage === "es" && detections.language === "es"
          ? "en"
          : "es"
        : originLanguage;

    let [translations] = await translate.translate(text, target);
    translations = Array.isArray(translations) ? translations : [translations];
    translations.forEach((translation, i) => {
      res
        .status(200)
        .json({
          text: translation,
          target: target,
          detections: detections.language,
          success: true,
        })
        .end();
    });
  }

  translateText();
});

export default routerTranslation;
