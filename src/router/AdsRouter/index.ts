import { Router, Request, Response } from "express";
import lastViewSchema from "../../models/lastView";

const routerAds = Router();

routerAds.post("/created-view", async (req, res: Response) => {
  const { adsID, userID } = req.body;
  const isExist = await lastViewSchema.findOne({
    adsID: adsID,
    userID: userID,
  });
  if (isExist) {
    console.log("You have already visited this ad");
    res
      .status(200)
      .json({
        messages: "You have already visited this ad",
        success: true,
        status: 200,
      })
      .end();
  } else {
    const newLastView = new lastViewSchema({
      adsID,
      userID,
    });

    newLastView.save((error: any) => {
      if (error) {
        res
          .status(400)
          .json({
            messages: "Algo salio mal",
            success: true,
            status: 200,
          })
          .end();
      } else {
        console.log("Vista creada");
        res
          .status(200)
          .json({
            messages: "Creado con éxito",
            success: true,
            status: 200,
          })
          .end();
      }
    });
  }
});

export default routerAds;
