import express, { Request, Response, NextFunction } from "express";
import morgan from "morgan";
import mainRouter from "./router/index.router";
import loginRouter from "./Login";
import helmet from "helmet";
import compression from "compression";
import cors from "cors";
import phoneRouter from "./Twilio/ConfirmNumber";
import emailRouter from "./Emails/recoverPassword";
import routerTranslation from "./router/translation";
import routerStripe from "./router/Stripe";
import routerPaypal from "./router/Paypal";
import engines from "consolidate";
import routerConverter from "./router/CurrencyConverter";
import routerNotification from "./router/Notification/index";
import routerAds from "./router/AdsRouter";
import routerMarke from "./router/Marke";
import { functions } from "./funtions";
import routerCupon from "./router/Cupon";
import chatRouter from "./router/Chats";
import routerUser from "./router/user";
import routerVerify from "./router/Verify";
const Sentry = require("@sentry/node");
const Tracing = require("@sentry/tracing");

Sentry.init({
  dsn: "https://3ac4b4a43dbe44f89cbae1a2f17a5c3a@o1200910.ingest.sentry.io/6449327",
  tracesSampleRate: 1.0,
});

const app = express();
app.use(cors({ origin: "*" }));
app.set("port", process.env.PORT || 4001);
app.use(morgan("dev"));
app.use(
  helmet({
    contentSecurityPolicy:
      process.env.NODE_ENV === "production" ? undefined : false,
  })
);

functions();

app.use(compression());
app.use(
  //@ts-ignore
  express.urlencoded({
    extended: true,
    limit: "500mb",
    parameterLimit: 500000,
  })
);
//@ts-ignore
app.use(express.json({ limit: "50mb", type: "application/json" }));

app.use(function (req: Request, res: Response, next: NextFunction) {
  //@ts-ignore
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Origin", req.headers.origin);
  res.header("Access-Control-Allow-Methods", "OPTIONS,GET,PUT,POST,DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, X-XSRF-TOKEN"
  );

  next();
});

app.engine("ejs", engines.ejs);
app.set("views", "views");
app.set("view engine", "ejs");

app.use(mainRouter);
app.use(loginRouter);
app.use(phoneRouter);
app.use(emailRouter);
app.use(routerTranslation);
app.use(routerStripe);
app.use(routerPaypal);
app.use(routerConverter);
app.use(routerNotification);
app.use(routerAds);
app.use(routerMarke);
app.use(routerCupon);
app.use(chatRouter);
app.use(routerUser);
app.use(routerVerify);

export default app;
