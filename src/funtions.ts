import adsSchema from "./models/ads";
import selectMarksSchema from "./models/selectMark";
import { markedata } from "./mark";
import phoneSchema from "./models/Phone";
import { phones } from "./phone";
var request = require("request");

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

const dat = [
  {
    id: 1,
    name: "Aeropuerto Internacional de Punta Cana (PUJ)",
    city: "Punta Cana",
    code: "PUJ",
    available: true,
  },
  {
    id: 2,
    name: "Aeropuerto Internacional Las Américas (SDQ)",
    city: "Santo Domingo",
    code: "SDQ",
    available: true,
  },
  {
    id: 3,
    name: "Aeropuerto Internacional La Isabela (JBQ)",
    city: "Santo Domingo",
    code: "JBQ",
    available: true,
  },
  {
    id: 4,
    name: "Aeropuerto Internacional Gregorio Luperón(POP)",
    city: "Puerto Plata",
    code: "POP",
    available: true,
  },
  {
    id: 5,
    name: "Aeropuerto Internacional Presidente Juan Bosch(AZS)",
    city: "Samaná",
    code: "AZS",
    available: true,
  },
  {
    id: 6,
    name: "Aeropuerto Internacional del Cibao (STI)",
    city: "Santiago",
    code: "STI",
    available: true,
  },
  {
    id: 7,
    name: "Aeropuerto Internacional de La Romana (LRM)",
    city: "La Romana",
    code: "LRM",
    available: true,
  },
  {
    id: 7,
    name: "Aeropuerto Internacional María Montez (BRX)",
    city: "Barahona",
    code: "BRX",
    available: true,
  },
];

export const functions = async () => {
  /*
  var options = {
    method: "GET",
    url: "https://stationapi.veriff.com/v1/sessions/9453f15f-4d30-4923-9f6e-16a4fc0ea3c8/media",
    headers: {
      "Content-Type": "application/json",
      "X-HMAC-SIGNATURE":
        "1e42f10ebca9a73be2aada1f32335c1e6efa79925e51ae9729813276dcc5bf44",
      "X-AUTH-CLIENT": "59d2b46f-1a13-47ba-b2c0-71630d4cc040",
    },
  };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);

    //console.log(JSON.parse(body));
  });

  var optionss = {
    method: "GET",
    url: "https://stationapi.veriff.com/v1/media/5659d878-966a-4942-a897-8f3a1fa3358b",
    headers: {
      "Content-Type": "application/json",
      "X-AUTH-CLIENT": "59d2b46f-1a13-47ba-b2c0-71630d4cc040",
    },
  };

  request(optionss, function (error, response, body) {
    if (error) throw new Error(error);

    console.log(JSON.parse(body));
  });
 */
  const createAds = async () => {
    markedata().forEach((items, i) => {
      const childrems = items.models.map((item, y) => {
        const newModel = {
          label: item,
          value: y === 0 ? 345 : y,
        };

        return newModel;
      });

      const newMArk = {
        label: items.label,
        value: i === 0 ? 245 : i,
        models: childrems,
      };

      const newMark = new selectMarksSchema(newMArk);
      newMark.save(async (error, ads: any) => {
        if (error) {
        } else {
        }
      });
    });
  };

  const createPhone = async () => {
    phones().forEach((items, i) => {
      const newMArk = {
        name: items.name,
        code: items.code,
        dial_code: items.dial_code,
        flag: items.flag,
      };

      const newMark = new phoneSchema(newMArk);
      newMark.save(async (error, ads: any) => {
        if (error) {
          console.error(error);
        } else {
          console.log("save");
        }
      });
    });
  };

  const updateProd = async () => {
    const g = await adsSchema.find();
    g.forEach((x) => {
      adsSchema.updateMany(
        { _id: x._id },
        {
          geometry: {
            type: "Point",
            coordinates: [
              x.location.geometry.location.lat,
              x.location.geometry.location.lng,
            ],
          },
        },
        { upsert: true },
        () => {
          console.log("done");
        }
      );
    });
  };

  //updateProd();
};
