import app from "./app";
import { Server as WebSocketServer } from "socket.io";
import { SocketIO } from "./socket";
import http from "http";
import "./db/connect";
import GQservers from "./GraphQL/config";
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });

const server = http.createServer(app);

//@ts-ignore
GQservers.applyMiddleware({ app });

const serverHttp = server.listen(app.get("port"), () => {
  console.log(`server on port`, app.get("port"));
});

const io = new WebSocketServer(serverHttp);

//socket IO
SocketIO(io);
