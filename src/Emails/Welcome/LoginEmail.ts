const nodemailer = require("nodemailer");
import loginEmails from "../Templates/Login";

export const LoginEmail = (email: string, user: any, input: any) => {
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    service: "gmail",
    port: 465,
    secure: true,
    auth: {
      user: process.env.EMAIL_ADDRESS,
      pass: process.env.EMAIL_PASSWORD,
    },
  });
  const mailOptions = {
    from: process.env.EMAIL_ADDRESS,
    to: email ? email : "info@rentytapp.com",
    subject: "Nuevo inicio de sesión en tu cuenta | Rentyt",
    text: "Hemos detectado un nuevo inicio de sesión en tu cuenta",
    html: loginEmails(user, input),
  };

  transporter.sendMail(mailOptions);
};
