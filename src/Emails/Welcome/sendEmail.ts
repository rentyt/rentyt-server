const nodemailer = require("nodemailer");
import welcomeEmails from "../Templates/welcomeEmail";

export const welcomeEmail = (email: string) => {
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    service: "gmail",
    port: 465,
    secure: true,
    auth: {
      user: process.env.EMAIL_ADDRESS,
      pass: process.env.EMAIL_PASSWORD,
    },
  });
  const mailOptions = {
    from: process.env.EMAIL_ADDRESS,
    to: email ? email : "info@rentytapp.com",
    subject: "Rentyt | Bienvenido a Rentyt",
    text: "Descubre las mejores ofertas en Rentyt y reserva de forma segura",
    html: welcomeEmails(email),
  };

  transporter.sendMail(mailOptions);
};
