import moment from "moment";

const t = (user: any, input: any) => {
  return `<!DOCTYPE html>
<html
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:v="urn:schemas-microsoft-com:vml"
  xmlns:o="urn:schemas-microsoft-com:office:office"
>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>Rentyt | Inicio de sesión</title>
    <!--[if !mso]><!-- -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   
     <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />

        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;700;900&display=swap"
          rel="stylesheet"
        />

    <style>
      * {
        font-family: 'Poppins', sans-serif;
      }
      .mobileContent {
        display: none;
        font-family: 'Poppins', sans-serif;
      }

      h1,
      .h1 {
        font-size: 60px !important;
        line-height: 66px !important;
        font-family: 'Poppins', sans-serif;
      }
      h2,
      .h2 {
        font-size: 44px !important;
        line-height: 50px !important;
        font-family: 'Poppins', sans-serif;
      }
      .btn a:hover {
        background-color: #000000 !important;
        border-color: #000000 !important;
        font-family: 'Poppins', sans-serif;
      }
      .textcta a:hover {
        color: #000000 !important;
        font-family: 'Poppins', sans-serif;
      }
      p {
        margin: 0 !important;
        font-family: 'Poppins', sans-serif;
      }
      .divbox:hover,
      * [lang~="x-divbox"]:hover {
        background-color: #000000 !important;
        font-family: 'Poppins', sans-serif;
      }
      .boxfont:hover,
      * [lang~="x-boxfont"]:hover {
        color: #ffffff !important;
        font-family: 'Poppins', sans-serif;
      }
      @media (max-width: 414px) {
        .hide {
          display: none !important;
          font-family: 'Poppins', sans-serif;
        }
        .show {
          display: inline-block !important;
          width: auto !important;
          height: auto !important;
          overflow: visible !important;
          float: none !important;
          visibility: visible !important;
          border: none !important;
          padding-bottom: 0px !important;
          vertical-align: bottom !important;
          font-family: 'Poppins', sans-serif;
        }
        .show1 {
          display: block !important;
          max-height: none !important;
          font-family: 'Poppins', sans-serif;
        }
        h1,
        h2,
        .h1,
        .h2 {
          font-size: 34px !important;
          line-height: 40px !important;
          font-family: 'Poppins', sans-serif;
        }
        .mobfb414,
        .mobfb414 td {
          padding-left: 0 !important;
          padding-right: 0 !important;
          max-width: 414px !important;
          font-family: 'Poppins', sans-serif;
        }
        .mobmw414 {
          max-width: 414px !important;
          font-family: 'Poppins', sans-serif;
        }
        .mobileContent {
          display: block !important;
          font-family: 'Poppins', sans-serif;
        }
        .desktopContent {
          display: none !important;
          font-family: 'Poppins', sans-serif;
        }
      }
    </style>

    <style type="text/css">
      @media screen and (max-width: 699px) {
        .full,
        .t10of12,
        .t11of12,
        .t12of12,
        .t1of12,
        .t2of12,
        .t3of12,
        .t4of12,
        .t5of12,
        .t6of12,
        .t7of12,
        .t8of12,
        .t9of12 {
          width: 100% !important;
          max-width: none !important;
          font-family: 'Poppins', sans-serif;
        }
        a[x-apple-data-detectors] {
          color: inherit !important;
          text-decoration: none !important;
          font-size: inherit !important;
          font-family: inherit !important;
          font-weight: inherit !important;
          line-height: inherit !important;
          font-family: 'Poppins', sans-serif;
        }
        .headerTextLeft {
          text-align: left !important;
          font-family: 'Poppins', sans-serif;
        }
        .hide {
          display: none !important;
          font-family: 'Poppins', sans-serif;
        }
        .mp0 {
          padding: 0 !important;
          font-family: 'Poppins', sans-serif;
        }
      }
      a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        font-family: 'Poppins', sans-serif;
      }
    </style>
  </head>

  <body
    dir="ltr"
    style="
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%;
      background-color: #d6d6d5;
      margin: 0;
      min-width: 100%;
      padding: 0;
      width: 100%;
      font-family: 'Poppins', sans-serif;
    "
  >
    <span
      data-blockuuid="25ed175d-ca3c-4821-80b3-d903474864be"
      style="
        display: none;
        max-height: 0px;
        font-size: 0px;
        overflow: hidden;
        mso-hide: all;
        font-family: 'Poppins', sans-serif;
      "
    >
      Inicio de sesión en tu cuenta de Rentyt.
    </span>

    <style>
      .yahooHide {
        display: none !important;
        font-family: 'Poppins', sans-serif;
      }
    </style>
    <table
      width="100%"
      border="0"
      cellpadding="0"
      cellspacing="0"
      style="
        background-color: #d6d6d5;
        border: 0;
        border-collapse: collapse;
        border-spacing: 0;
        mso-table-lspace: 0;
        mso-table-rspace: 0;
      "
      bgcolor="#d6d6d5"
      class=""
    >
      <tbody>
        <tr>
          <td align="center" style="display: block">
            <!--[if (gte mso 9)|(IE)]>
<table width="700" align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
<![endif]-->
            <table
              width="100%"
              border="0"
              cellpadding="0"
              cellspacing="0"
              style="
                border: 0;
                border-collapse: collapse;
                border-spacing: 0;
                max-width: 700px;
                mso-table-lspace: 0;
                mso-table-rspace: 0;
              "
              class=""
            >
              <tbody>
                <tr>
                  <td style="background-color: #ffffff">
                    <!-- RB Header Master  cesfab_2018/09/30, edit 10.9.18  -->
                    <table
                      width="100%"
                      border="0"
                      cellpadding="0"
                      cellspacing="0"
                      style="
                        border: none;
                        border-collapse: collapse;
                        border-spacing: 0;
                        mso-table-lspace: 0;
                        mso-table-rspace: 0;
                        width: 100%;
                      "
                      data-blockuuid="cd2f1373-4470-4723-8b2d-aa1fa190ab6e"
                    >
                      <tbody>
                        <tr>
                          <td
                            class="outsidegutter"
                            align="left"
                            style="
                              direction: ltr;
                              text-align: left;
                              padding: 10px 14px 10px 14px;
                              padding-left: 0;
                              background-color: #1C1548;
                            "
                            bgcolor="#1C1548"
                          >
                            <table
                              border="0"
                              cellpadding="0"
                              cellspacing="0"
                              style="
                                border: none;
                                border-collapse: collapse;
                                border-spacing: 0;
                                mso-table-lspace: 0;
                                mso-table-rspace: 0;
                                width: 100%;
                              "
                              class=""
                            >
                              <tbody>
                                <tr>
                                  <td
                                    width="14"
                                    style="
                                      direction: ltr;
                                      text-align: left;
                                      padding-left: 0;
                                      padding-right: 0;
                                    "
                                  >
                                    <img
                                      src="https://s3.amazonaws.com/uber-static/emails/2016/10/halloweenrider/sp_12x12.png"
                                      width="14"
                                      height="54"
                                      style="
                                        -ms-interpolation-mode: bicubic;
                                        clear: both;
                                        display: block;
                                        max-width: 100%;
                                        outline: none;
                                        text-decoration: none;
                                      "
                                      border="0"
                                      alt=""
                                    />
                                  </td>

                                  <td
                                    style="
                                      direction: ltr;
                                      text-align: left;
                                      font-size: 0;
                                    "
                                  >
                                    <table
                                      border="0"
                                      cellpadding="0"
                                      cellspacing="0"
                                      class="t1of12"
                                      style="
                                        border: none;
                                        border-collapse: collapse;
                                        border-spacing: 0;
                                        max-width: 56px;
                                        mso-table-lspace: 0;
                                        mso-table-rspace: 0;
                                        width: 100%;
                                        display: inline-block;
                                        vertical-align: middle;
                                      "
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            style="
                                              direction: ltr;
                                              text-align: left;
                                              padding-left: 12px;
                                              padding-right: 12px;
                                            "
                                          >
                                            <table
                                              border="0"
                                              cellpadding="0"
                                              cellspacing="0"
                                              width="100%"
                                              align="left"
                                              style="
                                                border: none;
                                                border-collapse: collapse;
                                                border-spacing: 0;
                                                mso-table-lspace: 0;
                                                mso-table-rspace: 0;
                                                table-layout: fixed;
                                                width: 100%;
                                              "
                                            >
                                              <tbody>
                                                <tr>
                                                  <td
                                                    height="2"
                                                    style="
                                                      direction: ltr;
                                                      text-align: left;
                                                      font-size: 0;
                                                      line-height: 1px;
                                                    "
                                                  >
                                                    &nbsp;
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>

                                    <!--[if (gte mso 9)|(IE)]>
    </td>


    <td width="616">
  <![endif]-->

                                    <table
                                      border="0"
                                      cellpadding="0"
                                      cellspacing="0"
                                      class="t11of12"
                                      style="
                                        border: none;
                                        border-collapse: collapse;
                                        border-spacing: 0;
                                        max-width: 616px;
                                        mso-table-lspace: 0;
                                        mso-table-rspace: 0;
                                        width: 100%;
                                        display: inline-block;
                                        vertical-align: middle;
                                      "
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            style="
                                              direction: ltr;
                                              text-align: left;
                                              padding-left: 0;
                                              padding-right: 0;
                                            "
                                          >
                                            <table
                                              border="0"
                                              cellpadding="0"
                                              cellspacing="0"
                                              width="100%"
                                              align="left"
                                              style="
                                                border: none;
                                                border-collapse: collapse;
                                                border-spacing: 0;
                                                mso-table-lspace: 0;
                                                mso-table-rspace: 0;
                                                table-layout: fixed;
                                                width: 100%;
                                              "
                                            >
                                              <tbody>
                                                <tr>
                                                  <td
                                                    style="
                                                      direction: ltr;
                                                      text-align: left;
                                                      font-size: 0;
                                                    "
                                                  >
                                                    <!--[if (gte mso 9)|(IE)]>
              <table width="616" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
              <td width="408">
              <![endif]-->

                                                    <table
                                                      border="0"
                                                      cellpadding="0"
                                                      cellspacing="0"
                                                      class="t4of12"
                                                      style="
                                                        border: none;
                                                        border-collapse: collapse;
                                                        border-spacing: 0;
                                                        display: inline-block;
                                                        max-width: 408px;
                                                        mso-table-lspace: 0;
                                                        mso-table-rspace: 0;
                                                        vertical-align: middle;
                                                        width: 100%;
                                                      "
                                                    >
                                                      <tbody>
                                                        <tr>
                                                          <td
                                                            style="
                                                              direction: ltr;
                                                              text-align: left;
                                                              padding-left: 12px;
                                                              padding-right: 12px;
                                                            "
                                                          >
                                                            <table
                                                              border="0"
                                                              cellpadding="0"
                                                              cellspacing="0"
                                                              width="100%"
                                                              align="left"
                                                              style="
                                                                border: none;
                                                                border-collapse: collapse;
                                                                border-spacing: 0;
                                                                mso-table-lspace: 0;
                                                                mso-table-rspace: 0;
                                                                table-layout: fixed;
                                                                width: 100%;
                                                              "
                                                            >
                                                              <tbody>
                                                                <!-- U B E R - - - - - - -->
                                                                <tr>
                                                                  <td
                                                                    style="
                                                                      direction: ltr;
                                                                      text-align: left;
                                                                      font-size: 0;
                                                                    "
                                                                  >
                                                                    <table
                                                                      border="0"
                                                                      cellpadding="0"
                                                                      cellspacing="0"
                                                                      width="100%"
                                                                      align="left"
                                                                      style="
                                                                        border: none;
                                                                        border-collapse: collapse;
                                                                        border-spacing: 0;
                                                                        mso-table-lspace: 0;
                                                                        mso-table-rspace: 0;
                                                                        table-layout: fixed;
                                                                        width: 100%;
                                                                      "
                                                                    >
                                                                      <tbody>
                                                                        <tr>
                                                                          <td
                                                                            style="
                                                                              direction: ltr;
                                                                              text-align: left;
                                                                              font-size: 0;
                                                                              padding-top: 0px;
                                                                              padding-bottom: 0px;
                                                                            "
                                                                          >
                                                                            <img
                                                                              src="https://rentyt-internal.s3.eu-west-3.amazonaws.com/logo.png"
                                                                              width="95"
                                                                              height="43"
                                                                              alt="Uber"
                                                                              style="
                                                                                -ms-interpolation-mode: bicubic;
                                                                                clear: both;
                                                                                display: block;
                                                                                max-width: 100%;
                                                                                outline: none;
                                                                                text-decoration: none;
                                                                              "
                                                                            />
                                                                          </td>
                                                                        </tr>
                                                                      </tbody>
                                                                    </table>
                                                                  </td>
                                                                </tr>
                                                                <!-- END U B E R - - - - - - -->
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>

                                                    <!--[if (gte mso 9)|(IE)]>
</td>
<td width="208">
<![endif]-->

                                                    <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>

                        <!-- line under -->
                      </tbody>
                    </table>
                    <!-- close RB Header Master -->
                    <table
                      width="100%"
                      border="0"
                      cellpadding="0"
                      cellspacing="0"
                      style="
                        border: 0;
                        border-collapse: collapse;
                        border-spacing: 0;
                        margin: auto;
                        max-width: 700px;
                        mso-table-lspace: 0;
                        mso-table-rspace: 0;
                      "
                      class="tron"
                    >
                      <tbody>
                        <tr>
                          <td align="center">
                            <table
                              width="100%"
                              border="0"
                              cellpadding="0"
                              cellspacing="0"
                              style="
                                background-color: #fff;
                                border: 0;
                                border-collapse: collapse;
                                border-spacing: 0;
                                margin: auto;
                                mso-table-lspace: 0;
                                mso-table-rspace: 0;
                              "
                              bgcolor="#ffffff"
                              class="basetable"
                            >
                              <tbody>
                                <tr>
                                  <td align="center">
                                    <!--[if (gte mso 9)|(IE)]>
<table width="700" align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td align="center">
<![endif]-->
                                    <table
                                      width="100%"
                                      border="0"
                                      cellpadding="0"
                                      cellspacing="0"
                                      class="basetable"
                                      style="
                                        border: 0;
                                        border-collapse: collapse;
                                        border-spacing: 0;
                                        mso-table-lspace: 0;
                                        mso-table-rspace: 0;
                                      "
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            align="center"
                                            style="background-color: #ffffff"
                                          >
                                            <table
                                              border="0"
                                              cellpadding="0"
                                              cellspacing="0"
                                              width="100%"
                                              class="basetable"
                                              style="
                                                border: 0;
                                                border-collapse: collapse;
                                                border-spacing: 0;
                                                mso-table-lspace: 0;
                                                mso-table-rspace: 0;
                                              "
                                            >
                                              <tbody>
                                                <tr>
                                                  <td>
                                                    <!--[if (gte mso 9)|(IE)]>
<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
<![endif]-->
                                                    <table
                                                      width="100%"
                                                      border="0"
                                                      cellpadding="0"
                                                      cellspacing="0"
                                                      class="basetable"
                                                      style="
                                                        border: 0;
                                                        border-collapse: collapse;
                                                        border-spacing: 0;
                                                        mso-table-lspace: 0;
                                                        mso-table-rspace: 0;
                                                      "
                                                    >
                                                      <tbody>
                                                        <tr>
                                                          <td>
                                                            <!--[if (gte mso 9)|(IE)]>
<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
<![endif]-->
                                                            <table
                                                              width="100%"
                                                              border="0"
                                                              cellpadding="0"
                                                              cellspacing="0"
                                                              style="
                                                                border: 0;
                                                                border-collapse: collapse;
                                                                border-spacing: 0;
                                                                mso-table-lspace: 0;
                                                                mso-table-rspace: 0;
                                                              "
                                                              class=""
                                                            >
                                                              <tbody>
                                                                <tr>
                                                                  <td>
                                                                    <!-- header  mt_2019/03/22  -->
                                                                    <table
                                                                      width="100%"
                                                                      border="0"
                                                                      cellpadding="0"
                                                                      cellspacing="0"
                                                                      style="
                                                                        border: none;
                                                                        border-collapse: collapse;
                                                                        border-spacing: 0;
                                                                        mso-table-lspace: 0;
                                                                        mso-table-rspace: 0;
                                                                        width: 100%;
                                                                      "
                                                                      data-blockuuid="22c0de01-5b31-49d8-95e9-1512a841796f"
                                                                    >
                                                                      <tbody>
                                                                        <tr>
                                                                          <td
                                                                            class="xoutsidegutter"
                                                                            align="left"
                                                                            style="
                                                                              direction: ltr;
                                                                              text-align: left;
                                                                              background-color: #29d884;
                                                                            "
                                                                          >
                                                                            <table
                                                                              border="0"
                                                                              cellpadding="0"
                                                                              cellspacing="0"
                                                                              style="
                                                                                border: none;
                                                                                border-collapse: collapse;
                                                                                border-spacing: 0;
                                                                                mso-table-lspace: 0;
                                                                                mso-table-rspace: 0;
                                                                                width: 100%;
                                                                              "
                                                                              class=""
                                                                            >
                                                                              <tbody>
                                                                                <tr>
                                                                                  <td
                                                                                    align="right"
                                                                                    style="
                                                                                      direction: ltr;
                                                                                      text-align: left;
                                                                                    "
                                                                                  >
                                                                                    <!--[if (gte mso 9)|(IE)]>
<table width="56" align="left" cellpadding="0" cellspacing="0" border="0">
   <tr>
      <td>
         <![endif]-->
                                                                                    <table
                                                                                      border="0"
                                                                                      cellpadding="0"
                                                                                      cellspacing="0"
                                                                                      class="t1of12 layout"
                                                                                      align="left"
                                                                                      style="
                                                                                        border: none;
                                                                                        border-collapse: collapse;
                                                                                        border-spacing: 0;
                                                                                        max-width: 56px;
                                                                                        mso-table-lspace: 0;
                                                                                        mso-table-rspace: 0;
                                                                                        width: 100%;
                                                                                      "
                                                                                    >
                                                                                      <tbody>
                                                                                        <tr>
                                                                                          <td
                                                                                            style="
                                                                                              direction: ltr;
                                                                                              text-align: left;
                                                                                              font-size: 1px;
                                                                                              height: 1px;
                                                                                              line-height: 1px;
                                                                                              padding-left: 0px !important;
                                                                                              padding-right: 0px !important;
                                                                                            "
                                                                                          >
                                                                                            <table
                                                                                              border="0"
                                                                                              cellpadding="0"
                                                                                              cellspacing="0"
                                                                                              width="100%"
                                                                                              align="left"
                                                                                              style="
                                                                                                border: none;
                                                                                                border-collapse: collapse;
                                                                                                border-spacing: 0;
                                                                                                mso-table-lspace: 0;
                                                                                                mso-table-rspace: 0;
                                                                                                table-layout: fixed;
                                                                                                width: 100%;
                                                                                              "
                                                                                            >
                                                                                              <tbody>
                                                                                                <tr>
                                                                                                  <td
                                                                                                    style="
                                                                                                      direction: ltr;
                                                                                                      text-align: left;
                                                                                                    "
                                                                                                  >
                                                                                                    &nbsp;
                                                                                                  </td>
                                                                                                </tr>
                                                                                              </tbody>
                                                                                            </table>
                                                                                          </td>
                                                                                        </tr>
                                                                                      </tbody>
                                                                                    </table>
                                                                                    <!--[if (gte mso 9)|(IE)]>
      </td>
   </tr>
</table>
<![endif]-->
                                                                                    <!--[if mso]></td>
<td valign="top">
   <![endif]-->

                                                                                    <!--[if (gte mso 9)|(IE)]>
<table width="644" align="left" cellpadding="0" cellspacing="0" border="0">
   <tr>
      <td>
         <![endif]-->
                                                                                    <table
                                                                                      border="0"
                                                                                      cellpadding="0"
                                                                                      cellspacing="0"
                                                                                      class="t11of12 layout"
                                                                                      align="left"
                                                                                      style="
                                                                                        border: none;
                                                                                        border-collapse: collapse;
                                                                                        border-spacing: 0;
                                                                                        max-width: 644px;
                                                                                        mso-table-lspace: 0;
                                                                                        mso-table-rspace: 0;
                                                                                        width: 100%;
                                                                                      "
                                                                                    >
                                                                                      <tbody>
                                                                                        <tr>
                                                                                          <td
                                                                                            style="
                                                                                              direction: ltr;
                                                                                              text-align: left;
                                                                                              font-size: 1px;
                                                                                              height: 1px;
                                                                                              line-height: 1px;
                                                                                              padding-left: 0px !important;
                                                                                              padding-right: 0px !important;
                                                                                            "
                                                                                          >
                                                                                            <table
                                                                                              border="0"
                                                                                              cellpadding="0"
                                                                                              cellspacing="0"
                                                                                              width="100%"
                                                                                              align="left"
                                                                                              style="
                                                                                                border: none;
                                                                                                border-collapse: collapse;
                                                                                                border-spacing: 0;
                                                                                                direction: rtl;
                                                                                                mso-table-lspace: 0;
                                                                                                mso-table-rspace: 0;
                                                                                                table-layout: fixed;
                                                                                                width: 100%;
                                                                                              "
                                                                                            >
                                                                                              <tbody>
                                                                                                <tr>
                                                                                                  <td
                                                                                                    style="
                                                                                                      text-align: left;
                                                                                                      direction: rtl;
                                                                                                      font-size: 0px;
                                                                                                    "
                                                                                                    valign="top"
                                                                                                  >
                                                                                                    <!--[if (gte mso 9)|(IE)]>
<table width="224" cellpadding="0" cellspacing="0" border="0" style="display:inline-block;vertical-align:top;">
   <tr>
      <td valign="top">
         <![endif]-->
                                                                                                    <table
                                                                                                      border="0"
                                                                                                      cellpadding="0"
                                                                                                      cellspacing="0"
                                                                                                      class="t4of12 layout"
                                                                                                      style="
                                                                                                        border: none;
                                                                                                        border-collapse: collapse;
                                                                                                        border-spacing: 0;
                                                                                                        display: inline-block;
                                                                                                        max-width: 224px;
                                                                                                        mso-table-lspace: 0;
                                                                                                        mso-table-rspace: 0;
                                                                                                        vertical-align: top;
                                                                                                        width: 100%;
                                                                                                      "
                                                                                                    >
                                                                                                      <tbody>
                                                                                                        <tr>
                                                                                                          <td
                                                                                                            style="
                                                                                                              direction: ltr;
                                                                                                              text-align: left;
                                                                                                              font-size: 1px;
                                                                                                              height: 1px;
                                                                                                              line-height: 1px;
                                                                                                              padding-left: 0px !important;
                                                                                                              padding-right: 0px !important;
                                                                                                            "
                                                                                                          >
                                                                                                            <table
                                                                                                              border="0"
                                                                                                              cellpadding="0"
                                                                                                              cellspacing="0"
                                                                                                              width="100%"
                                                                                                              align="left"
                                                                                                              style="
                                                                                                                border: none;
                                                                                                                border-collapse: collapse;
                                                                                                                border-spacing: 0;
                                                                                                                mso-table-lspace: 0;
                                                                                                                mso-table-rspace: 0;
                                                                                                                table-layout: fixed;
                                                                                                                width: 100%;
                                                                                                              "
                                                                                                            >
                                                                                                              <tbody>
                                                                                                                <tr>
                                                                                                                  <td
                                                                                                                    align="right"
                                                                                                                    style="
                                                                                                                      direction: ltr;
                                                                                                                      text-align: right;
                                                                                                                    "
                                                                                                                  >
                                                                                                                    <img
                                                                                                                      src="https://rentyt-internal.s3.eu-west-3.amazonaws.com/icon-email.png"
                                                                                                                      width="159"
                                                                                                                      height=""
                                                                                                                      style="
                                                                                                                        -ms-interpolation-mode: bicubic;
                                                                                                                        clear: both;
                                                                                                                        display: block;
                                                                                                                        height: auto;
                                                                                                                        margin: 0
                                                                                                                          0
                                                                                                                          0
                                                                                                                          auto;
                                                                                                                        max-width: 159px;
                                                                                                                        outline: none;
                                                                                                                        text-decoration: none;
                                                                                                                        width: 100%;
                                                                                                                      "
                                                                                                                      border="0"
                                                                                                                      alt=""
                                                                                                                    />
                                                                                                                  </td>
                                                                                                                </tr>
                                                                                                              </tbody>
                                                                                                            </table>
                                                                                                          </td>
                                                                                                        </tr>
                                                                                                      </tbody>
                                                                                                    </table>
                                                                                                    <!--[if (gte mso 9)|(IE)]>
      </td>
   </tr>
</table>
<![endif]-->
                                                                                                    <!--[if mso]></td>
<td valign="top">
   <![endif]-->

                                                                                                    <!--[if (gte mso 9)|(IE)]>
<table width="420" cellpadding="0" cellspacing="0" border="0" style="display:inline-block;vertical-align:top;">
   <tr>
      <td>
         <![endif]-->
                                                                                                    <table
                                                                                                      border="0"
                                                                                                      cellpadding="0"
                                                                                                      cellspacing="0"
                                                                                                      class="t7of12"
                                                                                                      style="
                                                                                                        border: none;
                                                                                                        border-collapse: collapse;
                                                                                                        border-spacing: 0;
                                                                                                        display: inline-block;
                                                                                                        max-width: 420px;
                                                                                                        mso-table-lspace: 0;
                                                                                                        mso-table-rspace: 0;
                                                                                                        vertical-align: top;
                                                                                                        width: 100%;
                                                                                                      "
                                                                                                    >
                                                                                                      <tbody>
                                                                                                        <tr>
                                                                                                          <td
                                                                                                            style="
                                                                                                              direction: ltr;
                                                                                                              text-align: left;
                                                                                                              padding-bottom: 35px;
                                                                                                              padding-left: 26px;
                                                                                                              padding-right: 26px;
                                                                                                              padding-top: 35px;
                                                                                                            "
                                                                                                          >
                                                                                                            <table
                                                                                                              border="0"
                                                                                                              cellpadding="0"
                                                                                                              cellspacing="0"
                                                                                                              width="100%"
                                                                                                              align="left"
                                                                                                              style="
                                                                                                                border: none;
                                                                                                                border-collapse: collapse;
                                                                                                                border-spacing: 0;
                                                                                                                mso-table-lspace: 0;
                                                                                                                mso-table-rspace: 0;
                                                                                                                table-layout: fixed;
                                                                                                                width: 100%;
                                                                                                              "
                                                                                                            >
                                                                                                              <tbody>
                                                                                                                <tr>
                                                                                                                  <td
                                                                                                                    class="h2 white"
                                                                                                                    style="
                                                                                                                      direction: ltr;
                                                                                                                      text-align: left;
                                                                                                                      color: #ffffff;
                                                                                                                      font-family: 'UberMove-Medium',
                                                                                                                        'HelveticaNeue',
                                                                                                                        Helvetica,
                                                                                                                        Arial,
                                                                                                                        sans-serif;
                                                                                                                      font-size: 34px;
                                                                                                                      line-height: 40px;
                                                                                                                      padding-bottom: 15px;
                                                                                                                      padding-top: 7px;
                                                                                                                    "
                                                                                                                  >
                                                                                                                    Hemos
                                                                                                                    detectado
                                                                                                                    un
                                                                                                                    nuevo
                                                                                                                    inicio
                                                                                                                    de
                                                                                                                    sesión
                                                                                                                    en
                                                                                                                    tu
                                                                                                                    cuenta
                                                                                                                  </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                  
                                                                                                                </tr>
                                                                                                              </tbody>
                                                                                                            </table>
                                                                                                          </td>
                                                                                                        </tr>
                                                                                                      </tbody>
                                                                                                    </table>
                                                                                                    <!--[if (gte mso 9)|(IE)]>
      </td>
   </tr>
</table>
<![endif]-->
                                                                                                  </td>
                                                                                                </tr>
                                                                                              </tbody>
                                                                                            </table>
                                                                                          </td>
                                                                                        </tr>
                                                                                      </tbody>
                                                                                    </table>
                                                                                    <!--[if (gte mso 9)|(IE)]>
      </td>
   </tr>
</table>
<![endif]-->
                                                                                  </td>
                                                                                </tr>
                                                                              </tbody>
                                                                            </table>
                                                                          </td>
                                                                        </tr>
                                                                      </tbody>
                                                                    </table>
                                                                    <!-- close header -->

                                                                    <!-- mod1 mt_2019/03/22  -->
                                                                    <table
                                                                      width="100%"
                                                                      border="0"
                                                                      cellpadding="0"
                                                                      cellspacing="0"
                                                                      style="
                                                                        border: none;
                                                                        border-collapse: collapse;
                                                                        border-spacing: 0;
                                                                        mso-table-lspace: 0;
                                                                        mso-table-rspace: 0;
                                                                        width: 100%;
                                                                      "
                                                                      data-blockuuid="22c0de01-5b31-49d8-95e9-1512a841796f"
                                                                    >
                                                                      <tbody>
                                                                        <tr>
                                                                          <td
                                                                            class="outsidegutter"
                                                                            align="left"
                                                                            style="
                                                                              direction: ltr;
                                                                              text-align: left;
                                                                              padding: 0
                                                                                14px
                                                                                0
                                                                                14px;
                                                                            "
                                                                          >
                                                                            <table
                                                                              border="0"
                                                                              cellpadding="0"
                                                                              cellspacing="0"
                                                                              style="
                                                                                border: none;
                                                                                border-collapse: collapse;
                                                                                border-spacing: 0;
                                                                                mso-table-lspace: 0;
                                                                                mso-table-rspace: 0;
                                                                                width: 100%;
                                                                              "
                                                                              class=""
                                                                            >
                                                                              <tbody>
                                                                                <tr>
                                                                                  <td
                                                                                    style="
                                                                                      direction: ltr;
                                                                                      text-align: left;
                                                                                    "
                                                                                  >
                                                                                    <!--[if (gte mso 9)|(IE)]>
<table width="560" align="center" cellpadding="0" cellspacing="0" border="0">
   <tr>
      <td>
         <![endif]-->
                                                                                    <table
                                                                                      border="0"
                                                                                      cellpadding="0"
                                                                                      cellspacing="0"
                                                                                      class="t10of12"
                                                                                      align="center"
                                                                                      style="
                                                                                        margin: 0
                                                                                          auto;
                                                                                        border: none;
                                                                                        border-collapse: collapse;
                                                                                        border-spacing: 0;
                                                                                        max-width: 560px;
                                                                                        mso-table-lspace: 0;
                                                                                        mso-table-rspace: 0;
                                                                                        width: 100%;
                                                                                      "
                                                                                    >
                                                                                      <tbody>
                                                                                        <tr>
                                                                                          <td
                                                                                            style="
                                                                                              direction: ltr;
                                                                                              text-align: left;
                                                                                              padding-left: 12px;
                                                                                              padding-right: 12px;
                                                                                            "
                                                                                          >
                                                                                            <table
                                                                                              border="0"
                                                                                              cellpadding="0"
                                                                                              cellspacing="0"
                                                                                              width="100%"
                                                                                              align="left"
                                                                                              style="
                                                                                                border: none;
                                                                                                border-collapse: collapse;
                                                                                                border-spacing: 0;
                                                                                                mso-table-lspace: 0;
                                                                                                mso-table-rspace: 0;
                                                                                                table-layout: fixed;
                                                                                                width: 100%;
                                                                                              "
                                                                                            >

                                                                                             <td
                                                                                                    class="p2"
                                                                                                    style="
                                                                                                      direction: ltr;
                                                                                                      text-align: left;

                                                                                                      font-weight: 600;
                                                                                                      color: #000000;
                                                                                                      font-family: 'Poppins', sans-serif;
                                                                                                      font-size: 22px;
                                                                                                      line-height: 22px;
                                                                                                      padding-bottom: 15px;
                                                                                                      padding-top: 30px;
                                                                                                    "
                                                                                                  >

                                                                                        
                                                                                      
                                                                                                   ¡Hola ${
                                                                                                     user.name
                                                                                                   }!
                                                                                                  </td>
                                                                                              <tbody>
                                                                                                <tr>

                                                                                                 

                                                                                  
                                                                                                  <td
                                                                                                    class="p2"
                                                                                                    style="
                                                                                                      direction: ltr;
                                                                                                      text-align: left;
                                                                                                      color: #000000;
                                                                                                      font-family: 'Poppins', sans-serif;
                                                                                                      font-size: 16px;
                                                                                                      line-height: 22px;
                                                                                                      padding-bottom: 35px;
                                                                                                      padding-top: 10px;
                                                                                                    "
                                                                                                  >

                                                                                        
                                                                                      
                                                                                                   Hemos detectado un nuevo inicio de sesión en tu cuenta y queremos asegurarnos de que has sido tú
                                                                                                  </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                  <td
                                                                                                    style="
                                                                                                      direction: ltr;
                                                                                                      text-align: left;
                                                                                                    "
                                                                                                  >
                                                                                        
                                                                                                </td>
                                                                                                </tr>
                                                                                              </tbody>
                                                                                            </table>
                                                                                          </td>
                                                                                        </tr>
</tbody>
</table>
 </td>
                                                                                </tr>
                                                                              </tbody>
                                                                            </table>
                                                                          </td>
                                                                        </tr>
                                                                      </tbody>
                                                                    </table>
                                                                    <!-- close mod1 mt_2019/03/22  -->

                                                                    <!-- list mt_2019/03/22  -->
                                                                    <table
                                                                      width="100%"
                                                                      border="0"
                                                                      cellpadding="0"
                                                                      cellspacing="0"
                                                                      style="
                                                                        border: none;
                                                                        border-collapse: collapse;
                                                                        border-spacing: 0;
                                                                        mso-table-lspace: 0;
                                                                        mso-table-rspace: 0;
                                                                        width: 100%;
                                                                      "
                                                                      data-blockuuid="22c0de01-5b31-49d8-95e9-1512a841796f"
                                                                    >
                                                                      <tbody>
                                                                        <tr>
                                                                          <td
                                                                            class="outsidegutter"
                                                                            align="left"
                                                                            style="
                                                                              direction: ltr;
                                                                              text-align: left;
                                                                              padding: 0
                                                                                14px
                                                                                0
                                                                                14px;
                                                                              padding-bottom: 45px;
                                                                              padding-top: 30px;
                                                                            "
                                                                          >
                                                                            <table
                                                                              border="0"
                                                                              cellpadding="0"
                                                                              cellspacing="0"
                                                                              style="
                                                                                border: none;
                                                                                border-collapse: collapse;
                                                                                border-spacing: 0;
                                                                                mso-table-lspace: 0;
                                                                                mso-table-rspace: 0;
                                                                                width: 100%;
                                                                              "
                                                                              class=""
                                                                            >
                                                                              <tbody>
                                                                                <tr>
                                                                                  <td
                                                                                    style="
                                                                                      direction: ltr;
                                                                                      text-align: left;
                                                                                    "
                                                                                  >
                                                                                    <!--[if (gte mso 9)|(IE)]>
<table width="560" align="center" cellpadding="0" cellspacing="0" border="0">
   <tr>
      <td>
         <![endif]-->
                                                                                    <table
                                                                                      border="0"
                                                                                      cellpadding="0"
                                                                                      cellspacing="0"
                                                                                      class="t10of12 layout"
                                                                                      align="center"
                                                                                      style="
                                                                                        margin: 0
                                                                                          auto;
                                                                                        border: none;
                                                                                        border-collapse: collapse;
                                                                                        border-spacing: 0;
                                                                                        max-width: 560px;
                                                                                        mso-table-lspace: 0;
                                                                                        mso-table-rspace: 0;
                                                                                        width: 100%;
                                                                                      "
                                                                                    >
                                                                                      <tbody>
                                                                                        <tr>
                                                                                          <td
                                                                                            style="
                                                                                              direction: ltr;
                                                                                              text-align: left;
                                                                                              font-size: 1px;
                                                                                              height: 1px;
                                                                                              line-height: 1px;
                                                                                              padding-left: 0px !important;
                                                                                              padding-right: 0px !important;
                                                                                            "
                                                                                          >
                                                                                            <table
                                                                                              border="0"
                                                                                              cellpadding="0"
                                                                                              cellspacing="0"
                                                                                              width="100%"
                                                                                              align="left"
                                                                                              style="
                                                                                                border: none;
                                                                                                border-collapse: collapse;
                                                                                                border-spacing: 0;
                                                                                                mso-table-lspace: 0;
                                                                                                mso-table-rspace: 0;
                                                                                                table-layout: fixed;
                                                                                                width: 100%;
                                                                                              "
                                                                                            >
                                                                                              <tbody>
                                                                                                <tr>
                                                                                                  <td
                                                                                                    style="
                                                                                                      direction: ltr;
                                                                                                      text-align: left;
                                                                                                    "
                                                                                                  >
                                                                                                    <!-- content  mt_2019/03/22  -->
                                                                                                    <table
                                                                                                      width="100%"
                                                                                                      border="0"
                                                                                                      cellpadding="0"
                                                                                                      cellspacing="0"
                                                                                                      style="
                                                                                                        border: none;
                                                                                                        border-collapse: collapse;
                                                                                                        border-spacing: 0;
                                                                                                        mso-table-lspace: 0;
                                                                                                        mso-table-rspace: 0;
                                                                                                        width: 100%;
                                                                                                      "
                                                                                                    >
                                                                                                      <tbody>
                                                                                                        <tr>
                                                                                                          <td
                                                                                                            class="xoutsidegutter"
                                                                                                            align="left"
                                                                                                            style="
                                                                                                              direction: ltr;
                                                                                                              text-align: left;
                                                                                                              padding-top: 15px;
                                                                                                              padding-bottom: 15px;
                                                                                                            "
                                                                                                          >

                                                                                          <table width="448" align="left" cellpadding="0" cellspacing="0" border="0">
   <tr>
      <td>
         <table border="0" cellpadding="0" cellspacing="0" class="t8of12" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 448px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
            <tbody><tr>
               <td style="direction:ltr;text-align:left;padding-left: 12px; padding-right: 12px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                     <tbody><tr>
                        <td class="h5" style="direction:ltr;text-align:left;color: #000000; font-family: 'UberMove-Medium', 'HelveticaNeue', Helvetica, Arial, sans-serif; font-size: 24px; line-height: 30px; padding-top: 0px;">Fecha y hora:</td>
                     </tr>
                     <tr>
                       <td class="p2" style="direction:ltr;text-align:left;color: #000000; font-family: 'Poppins', sans-serif; font-size: 16px; line-height: 22px; padding-bottom: 7px;">${moment(
                         input.date
                       ).format("LLL")}</td>
                     </tr>
                  </tbody></table>
               </td>
            </tr>
         </tbody>
        </table>


  <table width="448" align="left" cellpadding="0" cellspacing="0" border="0" style="margin-top: 1.5rem;">
   <tr>
      <td>
         <table border="0" cellpadding="0" cellspacing="0" class="t8of12" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 448px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
            <tbody><tr>
               <td style="direction:ltr;text-align:left;padding-left: 12px; padding-right: 12px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                     <tbody><tr>
                        <td class="h5" style="direction:ltr;text-align:left;color: #000000; font-family: 'UberMove-Medium', 'HelveticaNeue', Helvetica, Arial, sans-serif; font-size: 24px; line-height: 30px; padding-top: 0px;">Dispositivo:</td>
                     </tr>
                     <tr>
                       <td class="p2" style="direction:ltr;text-align:left;color: #000000; font-family: 'Poppins', sans-serif; font-size: 16px; line-height: 22px; padding-bottom: 7px;">${
                         input.Brand
                       } ${input.model}</td>
                     </tr>
                  </tbody></table>
               </td>
            </tr>
         </tbody>
        </table>


  <table width="448" align="left" cellpadding="0" cellspacing="0" border="0" style="margin-top: 1.5rem;">
   <tr>
      <td>
         <table border="0" cellpadding="0" cellspacing="0" class="t8of12" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 448px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
            <tbody><tr>
               <td style="direction:ltr;text-align:left;padding-left: 12px; padding-right: 12px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                     <tbody><tr>
                        <td class="h5" style="direction:ltr;text-align:left;color: #000000; font-family: 'UberMove-Medium', 'HelveticaNeue', Helvetica, Arial, sans-serif; font-size: 24px; line-height: 30px; padding-top: 0px;">Sistema operativo:</td>
                     </tr>
                     <tr>
                       <td class="p2" style="direction:ltr;text-align:left;color: #000000; font-family: 'Poppins', sans-serif; font-size: 16px; line-height: 22px; padding-bottom: 7px;">${
                         input.SystemName
                       }</td>
                     </tr>
                  </tbody>
                </table>
               </td>
            </tr>
         </tbody>
        </table>


  <table width="448" align="left" cellpadding="0" cellspacing="0" border="0" style="margin-top: 1.5rem;">
   <tr>
      <td>
         <table border="0" cellpadding="0" cellspacing="0" class="t8of12" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 448px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
            <tbody><tr>
               <td style="direction:ltr;text-align:left;padding-left: 12px; padding-right: 12px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                     <tbody><tr>
                        <td class="h5" style="direction:ltr;text-align:left;color: #000000; font-family: 'UberMove-Medium', 'HelveticaNeue', Helvetica, Arial, sans-serif; font-size: 24px; line-height: 30px; padding-top: 0px;">Ubicación:</td>
                     </tr>
                     <tr>
                       <td class="p2" style="direction:ltr;text-align:left;color: #000000; font-family: 'Poppins', sans-serif; font-size: 16px; line-height: 22px; padding-bottom: 7px;">${
                         input.countryCode
                       }</td>
                     </tr>
                  </tbody>
                </table>
               </td>
            </tr>
         </tbody>
        </table>


  <table width="448" align="left" cellpadding="0" cellspacing="0" border="0" style="margin-top: 1.5rem;">
   <tr>
      <td>
         <table border="0" cellpadding="0" cellspacing="0" class="t8of12" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 448px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
            <tbody><tr>
               <td style="direction:ltr;text-align:left;padding-left: 12px; padding-right: 12px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                     <tbody><tr>
                        <td class="h5" style="direction:ltr;text-align:left;color: #000000; font-family: 'Poppins', sans-serif; font-size: 24px; line-height: 30px; padding-top: 0px;">Dirección IP:</td>
                     </tr>
                     <tr>
                       <td class="p2" style="direction:ltr;text-align:left;color: #000000; font-family: 'Poppins', sans-serif; font-size: 16px; line-height: 22px; padding-bottom: 7px;">${
                         input.IpAddress
                       }</td>
                     </tr>
                  </tbody>
                </table>
               </td>
            </tr>
         </tbody>
        </table>
      </td>
   </tr>
</table>



<table width="448" align="left" cellpadding="0" cellspacing="0" border="0" style="margin-top: 2rem;">
   <tr>
      <td>
         <table border="0" cellpadding="0" cellspacing="0" class="t8of12" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; max-width: 448px; mso-table-lspace: 0; mso-table-rspace: 0; width: 100%;">
            <tbody><tr>
               <td style="direction:ltr;text-align:left;padding-left: 12px; padding-right: 12px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="left" style="border: none; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0; mso-table-rspace: 0; table-layout: fixed; width: 100%;">
                     <tbody>
                     <tr>
                       <td class="p2" style="direction:ltr;text-align:left;color: #000000; font-family: 'Poppins', sans-serif; font-size: 16px; line-height: 22px; padding-bottom: 7px;">Si no has sido tú, restablece la contraseña</td>
                     </tr>
                  </tbody>
                </table>
               </td>
            </tr>
         </tbody>
        </table>
      </td>
   </tr>
</table>

                                                                                                         
                                                                                                            
                                                                                                          </td>
                                                                                                        </tr>
                                                                                                      </tbody>
                                                                                                    </table>
                                                                                                  </tr>

                                                                                      
                                                                                                <tr>

                                                                                                  
                                                                                                  <td
                                                                                                    style="
                                                                                                      direction: ltr;
                                                                                                      text-align: left;
                                                                                                      padding-top: 10px;
                                                                                                      padding-bottom: 45px;
                                                                                                      padding-left: 12px;
                                                                                                      padding-right: 12px;
                                                                                                    ">

                                                                                      

                                                                                        
                                                                                                    <div
                                                                                                      class="btn desktopContent"
                                                                                                      lang="x-btn"
                                                                                                      style="
                                                                                                        font-family: 'UberMoveText-Bold',
                                                                                        
                                                                                                          'HelveticaNeueMedium',
                                                                                                          'HelveticaNeue-Medium',
                                                                                                          'Helvetica Neue Medium',
                                                                                                          Helvetica,
                                                                                                          Arial,
                                                                                                          sans-serif;
                                                                                                        font-size: 16px;
                                                                                                        line-height: 22px;
                                                                                                      "
                                                                                                    >
                                                                                                      <a                                                                                       href="https://rentytapp.com"
                                                                                                        style="
                                                                                                          background-color: #1C1548;

                                                                                                          font-family: 'Poppins', sans-serif;
                                                                                                          border-color: #1C1548;
                                                                                                          border-radius: 100px;
                                                                                                          border-style: solid;
                                                                                                          border-width: 13px
                                                                                                            18px;
                                                                                                          color: #ffffff;
                                                                                                          display: inline-block;
                                                                                                          letter-spacing: 1px;
                                                                                                          max-width: 300px;
                                                                                                          min-width: 150px;
                                                                                                          text-align: center;
                                                                                                          text-decoration: none;
                                                                                                          transition: all
                                                                                                            0.2s
                                                                                                            ease-in;
                                                                                                        "
                                                                                                        ><span style="float: left; text-align: left; font-family: 'Poppins', sans-serif;">Cambiar contraseña</span>
          <span
                                                                                                          style="
                                                                                                            float: right;
                                                                                                            padding-top: 3px;
                                                                                                            display: inline-block;
                                                                                                          "
                                                                                                        >
                                                                                                          <img
                                                                                                            src="https://s3.amazonaws.com/uber-static/emails/2018/global/arrows/arrow2_white_left.png"
                                                                                                            width="14"
                                                                                                            height="13"
                                                                                                            style="
                                                                                                              -ms-interpolation-mode: bicubic;
                                                                                                              margin-left: 7px;
                                                                                                              border: none;
                                                                                                              clear: both;
                                                                                                              display: block;
                                                                                                              margin-top: 2px;
                                                                                                              max-width: 100%;
                                                                                                              outline: none;
                                                                                                              text-decoration: none;
                                                                                                            "
                                                                                                            alt=""
                                                                                                            class=""
                                                                                                        /></span>
                                                                                                        <!--<![endif]-->
                                                                                                      </a>
                                                                                                    </div>

                                                                                                    <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                                                                                  </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                  <td
                                                                                                    class="ftrLegal"
                                                                                                    style="
                                                                                                      direction: ltr;
                                                                                                      text-align: left;
                                                                                                      color: #000000;
                                                                                                      font-family: 'Poppins', sans-serif;
                                                                                                      font-size: 12px;
                                                                                                      line-height: 20px;
                                                                                                      padding-left: 12px;
                                                                                                      padding-right: 12px;
                                                                                                    "
                                                                                                  >
                                                                                                    <p style="font-family: 'Poppins', sans-serif;">
                                                                                         Recuerda, Rentyt nunca, te enviará un correo electrónico pidiéndote tus datos bancarios o de la tarjeta. Entonces, si recibes algo como esto, bórralo. Te recomendamos que cambie tu contraseña regularmente también.
                                                                                                    </p>
                                                                                                  </td>
                                                                                                </tr>
                                                                                              </tbody>
                                                                                            </table>
                                                                                          </td>
                                                                                        </tr>
                                                                                      </tbody>
                                                                                    </table>
                                                                                    <!--[if (gte mso 9)|(IE)]>
      </td>
   </tr>
</table>
<![endif]-->
                                                                                  </td>
                                                                                </tr>
                                                                              </tbody>
                                                                            </table>
                                                                          </td>
                                                                        </tr>
                                                                      </tbody>
                                                                    </table>
                                                                    <!-- close list mt_2019/03/22  -->
                                                                  </td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                            <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                    <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                            <!-- END LIST-->
                          </td>
                        </tr>
                        <!-- END BODY-->
                      </tbody>
                    </table>
                    <!-- footer update july 25 2019  cf   -->
                    <table
                      width="100%"
                      border="0"
                      cellpadding="0"
                      cellspacing="0"
                      style="
                        background-color: #000000;
                        border: none;
                        border-collapse: collapse;
                        border-spacing: 0;
                        mso-table-lspace: 0;
                        mso-table-rspace: 0;
                        width: 100%;
                      "
                      data-blockuuid="3274d7db-36f8-4908-9bfb-ad301d6d2d34"
                    >
                      <tbody>
                        <tr>
                          <td
                            class="outsidegutter"
                            align="left"
                            style="
                              direction: ltr;
                              text-align: left;
                              padding: 0 14px 0 14px;
                            "
                          >
                            <table
                              border="0"
                              cellpadding="0"
                              cellspacing="0"
                              style="
                                border: none;
                                border-collapse: collapse;
                                border-spacing: 0;
                                mso-table-lspace: 0;
                                mso-table-rspace: 0;
                                width: 100%;
                              "
                              class=""
                            >
                              <tbody>
                                <!-- Top half -->
                                <tr>
                                  <td
                                    style="
                                      direction: ltr;
                                      text-align: left;
                                      padding-top: 30px;
                                      padding-bottom: 30px;
                                    "
                                  >
                                    <!--[if (gte mso 9)|(IE)]>
<table width="560" align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
<![endif]-->
                                    <table
                                      border="0"
                                      cellpadding="0"
                                      cellspacing="0"
                                      class="t10of12"
                                      align="center"
                                      style="
                                        margin: 0 auto;
                                        border: none;
                                        border-collapse: collapse;
                                        border-spacing: 0;
                                        max-width: 560px;
                                        mso-table-lspace: 0;
                                        mso-table-rspace: 0;
                                        width: 100%;
                                      "
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            style="
                                              direction: ltr;
                                              text-align: left;
                                              padding-left: 0;
                                              padding-right: 0;
                                            "
                                          >
                                            <table
                                              border="0"
                                              cellpadding="0"
                                              cellspacing="0"
                                              width="100%"
                                              align="left"
                                              style="
                                                border: none;
                                                border-collapse: collapse;
                                                border-spacing: 0;
                                                direction: rtl;
                                                mso-table-lspace: 0;
                                                mso-table-rspace: 0;
                                                table-layout: fixed;
                                                width: 100%;
                                              "
                                            >
                                              <tbody>
                                                <tr>
                                                  <td
                                                    class="ignoreTd"
                                                    style="
                                                      font-size: 0;
                                                      text-align: left;
                                                    "
                                                  >
                                                    <!--[if (gte mso 9)|(IE)]>
<table width="560" align="left" cellpadding="0" cellspacing="0" border="0">
<tr>
<td width="224">
<![endif]-->
                                                    <table
                                                      border="0"
                                                      cellpadding="0"
                                                      cellspacing="0"
                                                      class="t5of12"
                                                      style="
                                                        border: none;
                                                        border-collapse: collapse;
                                                        border-spacing: 0;
                                                        direction: ltr;
                                                        display: inline-block;
                                                        max-width: 224px;
                                                        mso-table-lspace: 0;
                                                        mso-table-rspace: 0;
                                                        vertical-align: top;
                                                        width: 100%;
                                                      "
                                                    >
                                                      <tbody>
                                                        <tr>
                                                          <td
                                                            style="
                                                              direction: ltr;
                                                              text-align: left;
                                                              padding-left: 12px;
                                                              padding-right: 12px;
                                                            "
                                                          >
                                                            <table
                                                              border="0"
                                                              cellpadding="0"
                                                              cellspacing="0"
                                                              width="100%"
                                                              align="left"
                                                              style="
                                                                border: none;
                                                                border-collapse: collapse;
                                                                border-spacing: 0;
                                                                mso-table-lspace: 0;
                                                                mso-table-rspace: 0;
                                                                table-layout: fixed;
                                                                width: 100%;
                                                              "
                                                            >
                                                              <tbody>
                                                                <tr>
                                                                  <td
                                                                    style="
                                                                      direction: ltr;
                                                                      text-align: left;
                                                                    "
                                                                  ></td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                    <!--[if (gte mso 9)|(IE)]>
</td>
<td width="336">
<![endif]-->
                                                    <table
                                                      border="0"
                                                      cellpadding="0"
                                                      cellspacing="0"
                                                      class="t6of12"
                                                      style="
                                                        border: none;
                                                        border-collapse: collapse;
                                                        border-spacing: 0;
                                                        direction: ltr;
                                                        display: inline-block;
                                                        max-width: 336px;
                                                        mso-table-lspace: 0;
                                                        mso-table-rspace: 0;
                                                        vertical-align: top;
                                                        width: 100%;
                                                      "
                                                    >
                                                      <tbody>
                                                        <tr>
                                                          <td
                                                            style="
                                                              direction: ltr;
                                                              text-align: left;
                                                              padding-left: 0;
                                                              padding-right: 0;
                                                            "
                                                          >
                                                            <table
                                                              border="0"
                                                              cellpadding="0"
                                                              cellspacing="0"
                                                              width="100%"
                                                              align="left"
                                                              style="
                                                                border: none;
                                                                border-collapse: collapse;
                                                                border-spacing: 0;
                                                                mso-table-lspace: 0;
                                                                mso-table-rspace: 0;
                                                                table-layout: fixed;
                                                                width: 100%;
                                                              "
                                                            >
                                                              <tbody>
                                                                <tr>
                                                                  <td
                                                                    style="
                                                                      direction: ltr;
                                                                      text-align: left;
                                                                    "
                                                                  >
                                                                    <!--[if (gte mso 9)|(IE)]>
<table width="336" align="left" cellpadding="0" cellspacing="0" border="0">
<tr>
<td width="168">
<![endif]-->
                                                                    <table
                                                                      border="0"
                                                                      cellpadding="0"
                                                                      cellspacing="0"
                                                                      class="t3of12"
                                                                      align="left"
                                                                      style="
                                                                        border: none;
                                                                        border-collapse: collapse;
                                                                        border-spacing: 0;
                                                                        max-width: 168px;
                                                                        mso-table-lspace: 0;
                                                                        mso-table-rspace: 0;
                                                                        width: 100%;
                                                                      "
                                                                    >
                                                                      <tbody>
                                                                        <tr>
                                                                          <td
                                                                            style="
                                                                              direction: ltr;
                                                                              text-align: left;
                                                                              padding-left: 12px;
                                                                              padding-right: 12px;
                                                                            "
                                                                          >
                                                                            <table
                                                                              border="0"
                                                                              cellpadding="0"
                                                                              cellspacing="0"
                                                                              width="100%"
                                                                              align="left"
                                                                              style="
                                                                                border: none;
                                                                                border-collapse: collapse;
                                                                                border-spacing: 0;
                                                                                mso-table-lspace: 0;
                                                                                mso-table-rspace: 0;
                                                                                table-layout: fixed;
                                                                                width: 100%;
                                                                              "
                                                                            >
                                                                              <tbody>
                                                                                <tr>
                                                                                  <td
                                                                                    class="white"
                                                                                    style="
                                                                                      color: rgb(
                                                                                        0,
                                                                                        0,
                                                                                        0
                                                                                      );
                                                                                      font-family: 'Poppins', sans-serif;
                                                                                      
                                                                                      
                                                                                      font-size: 12px;
                                                                                      line-height: 18px;
                                                                                      padding: 3px
                                                                                        0px;
                                                                                      direction: ltr;
                                                                                      text-align: left;
                                                                                    "
                                                                                  >
                                                                                    <a
                                                                                      href="http://rentytapp.com/contacto"
                                                                                      style="
                                                                                        color: rgb(
                                                                                          255,
                                                                                          255,
                                                                                          255
                                                                                        );
                                                                                        text-decoration: none;
                                                                                        font-family: 'Poppins', sans-serif;
                                                                                      "
                                                                                      >Centro de ayuda</a
                                                                                    >
                                                                                  </td>
                                                                                </tr>
                                                                                <tr>
                                                                                  <td
                                                                                    class="white"
                                                                                    style="
                                                                                      color: rgb(
                                                                                        0,
                                                                                        0,
                                                                                        0
                                                                                      );
                                                                                      font-family: 'Poppins', sans-serif;
                                                                                      font-size: 12px;
                                                                                      line-height: 18px;
                                                                                      padding: 3px
                                                                                        0px;
                                                                                      direction: ltr;
                                                                                      text-align: left;
                                                                                    "
                                                                                  >
                                                                                    <a
                                                                                      href="https://rentytapp.com/use-condition"
                                                                                      style="

                                                                                      font-family: 'Poppins', sans-serif;
                                                                                        color: rgb(
                                                                                          255,
                                                                                          255,
                                                                                          255
                                                                                        );
                                                                                        text-decoration: none;
                                                                                      "
                                                                                      >Términos y condiciones</a
                                                                                    >
                                                                                  </td>
                                                                                </tr>

                                                                                <tr>
                                                                                  <td
                                                                                    class="white"
                                                                                    style="
                                                                                      color: rgb(
                                                                                        0,
                                                                                        0,
                                                                                        0
                                                                                      );
                                                                                      font-family: 'Poppins', sans-serif;
                                                                                      font-size: 12px;
                                                                                      line-height: 18px;
                                                                                      padding: 3px
                                                                                        0px;
                                                                                      direction: ltr;
                                                                                      text-align: left;
                                                                                    "
                                                                                  >
                                                                                    <a
                                                                                      href="https://rentytapp.com"
                                                                                      style="

                                                                                      font-family: 'Poppins', sans-serif;
                                                                                        color: rgb(
                                                                                          255,
                                                                                          255,
                                                                                          255
                                                                                        );
                                                                                        text-decoration: none;
                                                                                      "
                                                                                      >Rentyt</a
                                                                                    >
                                                                                  </td>
                                                                                </tr>
                                                                              </tbody>
                                                                            </table>
                                                                          </td>
                                                                        </tr>
                                                                      </tbody>
                                                                    </table>
                                                                    <!--[if (gte mso 9)|(IE)]>
</td>
<td width="168">
<![endif]-->
                                                                    <table
                                                                      border="0"
                                                                      cellpadding="0"
                                                                      cellspacing="0"
                                                                      class="t3of12"
                                                                      align="left"
                                                                      style="
                                                                        border: none;
                                                                        border-collapse: collapse;
                                                                        border-spacing: 0;
                                                                        max-width: 168px;
                                                                        mso-table-lspace: 0;
                                                                        mso-table-rspace: 0;
                                                                        width: 100%;
                                                                      "
                                                                    >
                                                                      <tbody>
                                                                        <tr>
                                                                          <td
                                                                            style="
                                                                              direction: ltr;
                                                                              text-align: left;
                                                                              padding-left: 12px;
                                                                              padding-right: 12px;
                                                                            "
                                                                          >
                                                                            <table
                                                                              border="0"
                                                                              cellpadding="0"
                                                                              cellspacing="0"
                                                                              width="100%"
                                                                              align="left"
                                                                              style="
                                                                                border: none;
                                                                                border-collapse: collapse;
                                                                                border-spacing: 0;
                                                                                mso-table-lspace: 0;
                                                                                mso-table-rspace: 0;
                                                                                table-layout: fixed;
                                                                                width: 100%;
                                                                              "
                                                                            >
                                                                              <tbody>
                                                                                <tr>
                                                                                  <td
                                                                                    class="white"
                                                                                    style="
                                                                                      color: rgb(
                                                                                        0,
                                                                                        0,
                                                                                        0
                                                                                      );
                                                                                      font-family: 'Poppins', sans-serif;
                                                                                      font-size: 12px;
                                                                                      line-height: 18px;
                                                                                      padding: 3px
                                                                                        0px;
                                                                                      direction: ltr;
                                                                                      text-align: left;
                                                                                    "
                                                                                  >
                                                                                    <a
                                                                                      href="https://rentytapp.com/privacity"
                                                                                      style="

                                                                                      font-family: 'Poppins', sans-serif;
                                                                                        color: rgb(
                                                                                          255,
                                                                                          255,
                                                                                          255
                                                                                        );
                                                                                        text-decoration: none;
                                                                                      "
                                                                                      >Política de privacidad</a
                                                                                    >
                                                                                  </td>
                                                                                </tr>
                                                                                <tr>
                                                                                  <td
                                                                                    class="white"
                                                                                    style="
                                                                                      color: rgb(
                                                                                        0,
                                                                                        0,
                                                                                        0
                                                                                      );
                                                                                      font-family: 'Poppins', sans-serif;
                                                                                      font-size: 12px;
                                                                                      line-height: 18px;
                                                                                      padding: 3px
                                                                                        0px;
                                                                                      direction: ltr;
                                                                                      text-align: left;
                                                                                    "
                                                                                  >
                                                                                    <a
                                                                                      href="mailto:info@rentytapp.com"
                                                                                      style="

                                                                                      font-family: 'Poppins', sans-serif;
                                                                                        text-decoration: none;
                                                                                        color: #ffffff;
                                                                                      "
                                                                                      >Darse de baja</a
                                                                                    >
                                                                                  </td>
                                                                                </tr>
                                                                              </tbody>
                                                                            </table>
                                                                          </td>
                                                                        </tr>
                                                                      </tbody>
                                                                    </table>
                                                                    <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                                                  </td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                    <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                  </td>
                                </tr>
                                <!-- END top half -->

                                <!-- bottom half -->
                                <tr>
                                  <td
                                    style="
                                      direction: ltr;
                                      text-align: left;
                                      padding-bottom: 30px;
                                    "
                                  >
                                    <!--[if (gte mso 9)|(IE)]>
<table width="560" align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td width="560">
<![endif]-->
                                    <table
                                      border="0"
                                      cellpadding="0"
                                      cellspacing="0"
                                      class="t10of12"
                                      align="center"
                                      style="
                                        margin: 0 auto;
                                        border: none;
                                        border-collapse: collapse;
                                        border-spacing: 0;
                                        max-width: 560px;
                                        mso-table-lspace: 0;
                                        mso-table-rspace: 0;
                                        width: 100%;
                                      "
                                    >
                                      <tbody>
                                        <tr>
                                          <td
                                            style="
                                              direction: ltr;
                                              text-align: left;
                                              padding-left: 0;
                                              padding-right: 0;
                                            "
                                          >
                                            <table
                                              border="0"
                                              cellpadding="0"
                                              cellspacing="0"
                                              width="100%"
                                              align="left"
                                              style="
                                                border: none;
                                                border-collapse: collapse;
                                                border-spacing: 0;
                                                direction: rtl;
                                                mso-table-lspace: 0;
                                                mso-table-rspace: 0;
                                                table-layout: fixed;
                                                width: 100%;
                                              "
                                            >
                                              <tbody>
                                                <tr>
                                                  <td
                                                    class="ignoreTd"
                                                    style="
                                                      font-size: 0;
                                                      text-align: left;
                                                    "
                                                  >
                                                    <!--[if (gte mso 9)|(IE)]>
<table width="560" align="left" cellpadding="0" cellspacing="0" border="0">
<tr>
<td width="224">
<![endif]-->
                                                    <table
                                                      border="0"
                                                      cellpadding="0"
                                                      cellspacing="0"
                                                      class="t4of12"
                                                      style="
                                                        border: none;
                                                        border-collapse: collapse;
                                                        border-spacing: 0;
                                                        direction: ltr;
                                                        display: inline-block;
                                                        max-width: 224px;
                                                        mso-table-lspace: 0;
                                                        mso-table-rspace: 0;
                                                        vertical-align: top;
                                                        width: 100%;
                                                      "
                                                    >
                                                      <tbody>
                                                        <tr>
                                                          <td
                                                            style="
                                                              direction: ltr;
                                                              text-align: left;
                                                              padding-left: 12px;
                                                              padding-right: 12px;
                                                            "
                                                          >
                                                            <table
                                                              border="0"
                                                              cellpadding="0"
                                                              cellspacing="0"
                                                              align="left"
                                                              style="
                                                                border: none;
                                                                border-collapse: collapse;
                                                                border-spacing: 0;
                                                                mso-table-lspace: 0;
                                                                mso-table-rspace: 0;
                                                                table-layout: fixed;
                                                              "
                                                            >
                                                              <tbody>
                                                                <tr>
                                                                  <td
                                                                    class="p3 white"
                                                                    style="
                                                                      padding-bottom: 12px;
                                                                      direction: ltr;
                                                                      text-align: left;
                                                                    "
                                                                  >
                                                                    <!-- social table -->
                                                                    <table
                                                                      border="0"
                                                                      cellpadding="0"
                                                                      cellspacing="0"
                                                                      width="100%"
                                                                      align="left"
                                                                      style="
                                                                        border: none;
                                                                        border-collapse: collapse;
                                                                        border-spacing: 0;
                                                                        mso-table-lspace: 0;
                                                                        mso-table-rspace: 0;
                                                                        table-layout: fixed;
                                                                        width: 130px;
                                                                      "
                                                                    >
                                                                      <tbody>
                                                                        <tr>
                                                                          <td
                                                                            width="43"
                                                                            align="center"
                                                                            style="
                                                                              direction: ltr;
                                                                              text-align: left;
                                                                            "
                                                                          >
                                                                            <a
                                                                              href="https://www.facebook.com/rentyt.do"
                                                                            >
                                                                              <img
                                                                                src="https://s3.amazonaws.com/uber-static/emails/2018/global/icons/08_28_18facebook.png"
                                                                                width="13"
                                                                                height=""
                                                                                border="0"
                                                                                style="
                                                                                  -ms-interpolation-mode: bicubic;
                                                                                  clear: both;
                                                                                  display: block;
                                                                                  height: auto;
                                                                                  max-height: 17px;
                                                                                  max-width: 13px;
                                                                                  outline: none;
                                                                                  text-decoration: none;
                                                                                  width: 100%;
                                                                                "
                                                                              />
                                                                            </a>
                                                                          </td>
                                                                          <td
                                                                            width="43"
                                                                            align="center"
                                                                            style="
                                                                              direction: ltr;
                                                                              text-align: left;
                                                                            "
                                                                          >
                                                                            <a
                                                                              href="https://twitter.com/rentyt_DO"
                                                                            >
                                                                              <img
                                                                                src="https://s3.amazonaws.com/uber-static/emails/2018/global/icons/08_28_18twitter.png"
                                                                                width="19"
                                                                                height=""
                                                                                border="0"
                                                                                style="
                                                                                  -ms-interpolation-mode: bicubic;
                                                                                  clear: both;
                                                                                  display: block;
                                                                                  height: auto;
                                                                                  max-height: 17px;
                                                                                  max-width: 19px;
                                                                                  outline: none;
                                                                                  text-decoration: none;
                                                                                  width: 100%;
                                                                                "
                                                                              />
                                                                            </a>
                                                                          </td>
                                                                          <td
                                                                            width="43"
                                                                            align="center"
                                                                            style="
                                                                              direction: ltr;
                                                                              text-align: left;
                                                                            "
                                                                          >
                                                                            <a
                                                                              href="https://www.instagram.com/rentyt.do/"
                                                                            >
                                                                              <img
                                                                                src="https://s3.amazonaws.com/uber-static/emails/2018/global/icons/08_28_18instagram.png"
                                                                                width="16"
                                                                                height=""
                                                                                border="0"
                                                                                style="
                                                                                  -ms-interpolation-mode: bicubic;
                                                                                  clear: both;
                                                                                  display: block;
                                                                                  height: auto;
                                                                                  max-height: 17px;
                                                                                  max-width: 16px;
                                                                                  outline: none;
                                                                                  text-decoration: none;
                                                                                  width: 100%;
                                                                                "
                                                                              />
                                                                            </a>
                                                                          </td>
                                                                        </tr>
                                                                      </tbody>
                                                                    </table>
                                                                    <!-- END social table -->
                                                                  </td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                    <!--[if (gte mso 9)|(IE)]>
</td>
<td width="336">
<![endif]-->
                                                    <table
                                                      border="0"
                                                      cellpadding="0"
                                                      cellspacing="0"
                                                      class="t6of12"
                                                      style="
                                                        border: none;
                                                        border-collapse: collapse;
                                                        border-spacing: 0;
                                                        direction: ltr;
                                                        display: inline-block;
                                                        max-width: 336px;
                                                        mso-table-lspace: 0;
                                                        mso-table-rspace: 0;
                                                        vertical-align: top;
                                                        width: 100%;
                                                      "
                                                    >
                                                      <tbody>
                                                        <tr>
                                                          <td
                                                            style="
                                                              direction: ltr;
                                                              text-align: left;
                                                              padding-left: 12px;
                                                              padding-right: 12px;
                                                            "
                                                          >
                                                            <table
                                                              border="0"
                                                              cellpadding="0"
                                                              cellspacing="0"
                                                              width="100%"
                                                              align="left"
                                                              style="
                                                                border: none;
                                                                border-collapse: collapse;
                                                                border-spacing: 0;
                                                                mso-table-lspace: 0;
                                                                mso-table-rspace: 0;
                                                                table-layout: fixed;
                                                                width: 100%;
                                                              "
                                                            >
                                                              <tbody>
                                                                <tr>
                                                                  <td
                                                                    class="p2"
                                                                    style="
                                                                      direction: ltr;
                                                                      text-align: left;
                                                                      color: #e5e5e5;
                                                                      font-family: 'Poppins', sans-serif;
                                                                      font-size: 10px;
                                                                      line-height: 18px;
                                                                    "
                                                                  >
                                                                    Este es un email el sistema de Rentyt por favor no contestar

                                                                    <br />

                                                                    Rentyt | Santo Domingo, República Dominicana

                                                                    <br />

                                                                    <a
                                                                      href="https://rentyt.com"
                                                                      style="
                                                                        text-decoration: none;
                                                                        color: #e5e5e5;
                                                                        font-family: 'Poppins', sans-serif;
                                                                      "
                                                                      >rentytapp.com</a
                                                                    >
                                                                  </td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                    <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                  </td>
                                </tr>
                                <!-- END bottom half -->
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <!-- close footer update - ces -->
                  </td>
                </tr>
              </tbody>
            </table>
            <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
`;
};

export default t;
