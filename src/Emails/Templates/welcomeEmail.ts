const t = (email: string) => {
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:v="urn:schemas-microsoft-com:vml"
  xmlns:o="urn:schemas-microsoft-com:office:office"
>
  <head>
    <title>Rentyt | Alquiler de vehículos</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width" />
    <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />

        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;700;900&display=swap"
          rel="stylesheet"
        />
    <style type="text/css">
      body {
        margin: 0 !important;
        padding: 0 !important;
        background-color: #f5f5f5;
      }
      table {
        border-spacing: 0 !important;
        border-collapse: collapse;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
      }
      td {
        padding: 0;
      }
      div[style*="margin: 16px 0"] {
        margin: 0 !important;
      }
      * {
        -webkit-text-size-adjust: none;
      }
      .ReadMsgBody {
        width: 100%;
      }
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass * {
        line-height: 100%;
      }
      input {
        display: none;
      }
      ul {
        padding: 0px !important;
      }
      .cta-mobile {
        transition: all 0.2s ease-in-out;
      }
      .cta-mobile:hover {
        opacity: 0.9;
      }
      /* Desktop settings */
      @media screen and (max-width: 9999px) {
        /* Recommendations Settings */
        .navbar-column-recommendations {
          padding-bottom: 404px !important;
        }
        .main-recommendation-hero {
          display: table !important;
        }
        .fallback-recommendation-hero {
          display: none !important;
        }
        .restaurant-cuisine-info-border {
          border-bottom-left-radius: 6px !important;
        }
        .restaurant-cuisine-image-border {
          border-bottom-right-radius: 6px !important;
        }
        /* Hide these two classes if there is an offer*/
        .restaurant-cuisine-border-no-voucher {
          border-top-right-radius: 6px !important;
        }
        .restaurant-cuisine-info-border-no-voucher {
          border-top-left-radius: 6px !important;
          border-top: 1px solid #eaeaea !important;
        }
        .mobile-2 {
          display: none;
        }
      }
    </style>
    <style type="text/css">
      /* Mobile Settings for Android */
      @media screen and (max-width: 639px) {
        /* Recommendations Settings */
        .restaurant-cuisine {
          max-width: 100% !important;
        }
        .restaurant-logo-container {
          display: block !important;
          width: 100% !important;
          padding-bottom: 10px;
        }
        .restaurant-info-container {
          display: block !important;
          width: 100% !important;
        }
        .recommendations-align-center {
          text-align: center !important;
        }
        .recommendation-restaurant-logo {
          margin: 0 auto !important;
        }
        td.restaurant-logo-container.recommendations-align-center {
          display: block !important;
          width: 100% !important;
          padding-bottom: 10px;
          text-align: center !important;
        }
        .recommendation-restaurant {
          height: auto !important;
        }
        .recommendation-discount-mobile {
          display: block !important;
        }
        .recommendation-discount-desktop {
          display: none !important;
        }
        .recommendation-block-border-odd {
          border-top: none !important;
        }
        .recommendation-block-border {
          border-left: 1px solid #eaeaea !important;
          border-top: 1px solid #eaeaea !important;
          border-right: 1px solid #eaeaea !important;
        }
        .white-caret-desktop {
          border-top: 3px solid #ffffff;
        }
        .white-caret-mobile {
          display: block !important;
        }
        .restaurant-cuisine-info-border {
          border-bottom-left-radius: 6px !important;
          border-bottom-right-radius: 6px !important;
        }
        .restaurant-cuisine-image-border {
          border-bottom-right-radius: 0 !important;
        }
      }
      /* Mobile Settings - XL Screens */
      @media screen and (max-width: 767px) {
        .mobile-navlinks {
          display: block !important;
        }
        /* Reorder Prompt CSS*/
        .mobile-image {
          width: 100% !important;
          max-width: 100% !important;
          height: auto !important;
        }
        .main-header {
          display: block !important;
        }
        .fallback-header {
          display: none !important;
        }
        .cta-mobile {
          max-width: 100% !important;
        }
        .show_image_on_mobile {
          display: block !important;
        }
        .hide_image_on_desktop {
          display: none !important;
        }
      }
    </style>
    <style type="text/css">
      /* Mobile Settings - Large Screens */
      @media only screen and (max-width: 480px) {
        .reco_1_padding_settings_mobile {
          padding-right: 20px !important;
        }
        .reco_2_padding_settings_mobile {
          padding-left: 20px !important;
        }
        .header_text {
          font-size: 36px !important;
          line-height: 36px !important;
        }
        .voucher_header_text {
          font-size: 40px !important;
          line-height: 40px !important;
        }
        .subheader_text {
          font-size: 21px !important;
          line-height: 21px !important;
        }
        .column-6-width {
          max-width: 160px !important;
        }
        .column-6-height {
          height: 140px !important;
        }
        .column-3-width {
          max-width: 200px !important;
        }
        .column-3-height {
          height: 180px !important;
        }
        .column-2-width {
          max-width: 160px !important;
        }
        .column-2-height {
          height: 140px !important;
        }
        .place-subheader {
          font-size: 20px !important;
        }
        .padding_inner {
          padding-left: 20px !important;
          padding-right: 20px !important;
        }
        .hero_container_inner {
          width: 20px !important;
        }
        .reco_details {
          padding-top: 20px !important;
          padding-bottom: 0px !important;
        }
        .voucher-code {
          font-size: 20px !important;
        }
        .ordernumber-tran {
          font-size: 21px !important;
        }
        .offer-title {
          font-size: 60px !important;
          line-height: 60px !important;
        }
        /* Targets both Gmail iOS and Gmail Android (Nexus, pixel) */
        u ~ div .full-wrap {
          min-width: 100vw;
        }
        /* Targets only Gmail Android */
        div > u ~ div .full-wrap {
          min-width: 100%;
        }
        /* Reorder Prompt Settings */
        .hide-spacer {
          display: none !important;
        }
        .cta-copy {
          font-size: 14px !important;
        }
        .headline-title {
          font-size: 32px !important;
        }
        .grand-total-copy {
          font-size: 26px !important;
        }
        .grand-total-amount {
          font-size: 26px !important;
        }
        .headline-footer {
          font-size: 32px !important;
        }
        .main-header {
          height: 320px !important;
        }
        .navbar-column {
          padding-bottom: 310px !important;
        }
        .align-center {
          text-align: center !important;
        }
        /* Recommendations Settings */
        .recommendation-hero {
          height: 320px !important;
          max-height: 320px !important;
        }
        .recommendation-title {
          font-size: 26px !important;
          padding-top: 20px !important;
          padding-left: 20px !important;
          padding-right: 20px !important;
        }
        /* Hides on mobile*/
        .desktop {
          display: none !important;
        }
        /* Shows on mobile */
        .mobile {
          display: block !important;
          width: 100% !important;
          max-height: inherit !important;
        }
        .mobile-2 {
          display: block !important;
          width: 100% !important;
          max-height: inherit !important;
        }
        /* Shows on Outlook */
        .hide-on-outlook {
          display: inline-block !important;
        }
        /* Sets the width of the container for mobile*/
        .fullWidth {
          width: 100% !important;
          min-width: 0px !important;
        }
      }
      /* Mobile Settings - Medium Screens */
      @media only screen and (max-width: 375px) {
        .column-6-width {
          max-width: 140px !important;
        }
        .column-6-height {
          height: 120px !important;
        }
        .column-2-width {
          max-width: 140px !important;
        }
        .column-2-height {
          height: 120px !important;
        }
        .placcement-copy {
          font-size: 14px !important;
        }
        /* Reorder Prompt CSS */
        .main-header {
          height: 250px !important;
        }
        .navbar-column {
          padding-bottom: 240px !important;
        }
        .align-center {
          text-align: center !important;
        }
        /* Recommendations Settings */
        .recommendation-hero {
          height: 250px !important;
          max-height: 250px !important;
        }
        .recommendation-title {
          font-size: 21px !important;
          padding-top: 10px !important;
          padding-left: 10px !important;
          padding-right: 10px !important;
        }
      }
      /* Samsung settings */
      @media screen and (min-width: 351px) and (max-width: 360px) {
        /* Sets the width of the container for mobile*/
        .fullWidth {
          width: 100% !important;
          min-width: 0px !important;
        }
      }
      /* iPhone 4 and 5 settings */
      @media screen and (max-width: 350px) {
        /* Sets the width of the container for mobile*/
        .fullWidth {
          width: 100% !important;
          min-width: 0px !important;
        }
      }
      /* Mobile Settings - Small Screens */
      @media only screen and (max-width: 320px) {
        .column-6-width {
          max-width: 130px !important;
        }
        .column-6-height {
          height: 110px !important;
        }
        .column-2-width {
          max-width: 130px !important;
        }
        .column-2-height {
          height: 110px !important;
        }
        .container-padding {
          padding-right: 10px !important;
          padding-left: 10px !important;
        }
        /* Targets both Gmail iOS and Gmail Android (Nexus, pixel) */
        u ~ div .full-wrap {
          min-width: 100vw;
        }
        /* Targets only Gmail Android */
        div > u ~ div .full-wrap {
          min-width: 100%;
        }
        /*Reorder Prompt CSS*/
        .headline-title {
          font-size: 32px !important;
        }
        .grand-total-copy {
          font-size: 26px !important;
        }
        .grand-total-amount {
          font-size: 26px !important;
        }
        .headline-footer {
          font-size: 32px !important;
        }
        .main-header {
          height: 213px !important;
        }
        /* Recommendations Settings */
        .recommendation-hero {
          height: 213px !important;
          max-height: 213px !important;
        }
        .recommendation-title {
          font-size: 18px !important;
          padding-top: 10px !important;
          padding-left: 10px !important;
          padding-right: 10px !important;
        }
        .restaurant-logo {
          width: 65px !important;
          height: auto !important;
        }
        .recommendation-discount {
          font-size: 18px !important;
        }
      }
      @media (max-device-width: 800px) and (-webkit-min-device-pixel-ratio: 0) and (min-resolution: 0.001dpcm) {
        /* Block for Samsung and Outlook Android */
        .mobile-2 {
          display: block !important;
          width: 100% !important;
          max-height: inherit !important;
        }
        .hide-on-samsung {
          display: none !important;
        }
      }
      @supports (-webkit-overflow-scrolling: touch) {
        /* CSS specific to iOS devices */
        .mobile-2 {
          display: none !important;
        }
      }
      @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
        .ie10up {
          property: value;
        }
        .mobile-2 {
          display: none !important;
        }
      }
    </style>
  </head>

  <!-- Preheader: ends -->
  <body style="background-color: #f5f5f5">
    <div
      style="
        display: none !important;
        visibility: hidden;
        mso-hide: all;
        font-size: 1px;
        color: #ffffff;
        line-height: 1px;
        max-height: 0px;
        max-width: 0px;
        opacity: 0;
        overflow: hidden;
      "
    >
      Descubre las mejores ofertas en Rentyt
    </div>
    <table
      width="100%"
      border="0"
      cellpadding="0"
      cellspacing="0"
      align="center"
      bgcolor="#f5f5f5"
    >
      <tr>
        <td align="center">
          <!--[if (gte mso 9)|(IE)]><table width="640" cellspacing="0" cellpadding="0" border="0" align="center"><tr><td><![endif]-->
          <table
            cellpadding="0"
            cellspacing="0"
            border="0"
            width="640"
            style="max-width: 640px; width: 100%"
          >
            <tr>
              <td align="center" style="font-size: 0">
                <table
                  cellpadding="0"
                  cellspacing="0"
                  border="0"
                  width="100%"
                  style="background-color: #ffffff"
                >
                  <tr>
                    <td>
                      <table
                        cellpadding="0"
                        cellspacing="0"
                        border="0"
                        width="100%"
                      >
                        <tr>
                          <td>
                            <table
                              border="0"
                              cellpadding="0"
                              cellspacing="0"
                              width="100%"
                            >
                              <tr>
                                <td style="display: none !important">
                                  <div
                                    style="
                                      display: none !important;
                                      max-height: 0px;
                                      overflow: hidden;
                                    "
                                  >
                                    &nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌&nbsp;‌
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td
                                  bgcolor="#f5f5f5"
                                  align="center"
                                  style="
                                    padding-top: 25px;
                                    padding-bottom: 20px;
                                  "
                                >
                                  <table
                                    cellpadding="0"
                                    cellspacing="0"
                                    border="0"
                                    width="100%"
                                  >
                                    <tr>
                                      <td align="center">
                                        <img
                                          src="https://rentyt.s3.eu-west-3.amazonaws.com/logo-rentyt-email.png"
                                          width="150"
                                          border="0"
                                          alt="Rentyt&#173;.co&#173;"
                                        />
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>

                              <!-- Header: ends -->

                              <!--
-->
                              <tr>
                                <td
                                  style="padding-bottom: 20px"
                                  bgcolor="#f5f5f5"
                                >
                                  <table
                                    cellpadding="0"
                                    cellspacing="0"
                                    border="0"
                                    width="100%"
                                    bgcolor="#ffffff"
                                  >
                                    <tr>
                                      <td
                                        align="left"
                                        style="
                                          padding-right: 20px;
                                          padding-bottom: 50px;
                                          padding-left: 20px;
                                          padding-top: 50px;
                                        "
                                      >
                                        <table
                                          cellpadding="0"
                                          cellspacing="0"
                                          border="0"
                                          width="100%"
                                        >
                                          <!--
-->
                                          <tr>
                                            <td
                                              align="center"
                                              style="padding-bottom: 20px"
                                            >
                                              <!--[if (gte mso 9)|(IE)]>
<table cellpadding="0" cellspacing="0" width="330">
<tr>
<td>
<![endif]-->

                                              <!--
-->
                                              <a
                                                href="rentytapp://app"
                                                target="_blank"
                                              >
                                                <img
                                                  src="https://rentyt.s3.eu-west-3.amazonaws.com/welcome.png"
                                                  width="330"
                                                  height="210"
                                                  border="0"
                                                  style="
                                                    display: block;
                                                    max-width: 330px;
                                                    width: 100%;
                                                    height: auto;
                                                  "
                                                  alt="Your account's set up, you're good to go."
                                                />
                                              </a>

                                              <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                            </td>
                                          </tr>

                                          <!--
-->
                                          <tr>
                                            <td align="center">
                                              <!--[if (gte mso 9)|(IE)]>
<table cellpadding="0" cellspacing="0" width="500">
<tr>
<td>
<![endif]-->
                                              <div
                                                style="
                                                  max-width: 500px;
                                                  width: 100%;
                                                "
                                              >
                                                <table
                                                  cellpadding="0"
                                                  cellspacing="0"
                                                  width="100%"
                                                  bgcolor="#ffffff"
                                                >
                                                  <tr>
                                                    <td
                                                      align="center"
                                                      style="
                                                        font-family: 'Poppins', sans-serif;
                                                        font-size: 40px;
                                                        color: rgb(
                                                          41,
                                                          216,
                                                          132
                                                        );
                                                        font-weight: 600;
                                                        mso-line-height-rule: exactly;
                                                        line-height: 48px;
                                                        padding-bottom: 20px;
                                                      "
                                                    >
                                                      Bienvenido a Rentyt
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td
                                                      align="center"
                                                      style="
                                                        font-family: 'Poppins', sans-serif;
                                                        font-size: 16px;
                                                        color: #53565a;
                                                        font-weight: 300;
                                                        mso-line-height-rule: exactly;
                                                        line-height: 24px;
                                                        padding-bottom: 20px;
                                                      "
                                                    >
                                                      <strong
                                                        >Encuentra las mejores
                                                        ofertas y reserva de
                                                        forma segura!</strong
                                                      >
                                                      <br />
                                                      <br />
                                                      Rentyt es la app que pone
                                                      en contacto a los rent car
                                                      de toda República
                                                      Dominicana con los
                                                      usuarios del mundo que
                                                      visitan nuestro país con
                                                      Rentyt hemos mejorado el
                                                      proceso de alquiler de
                                                      vehículo pudiendo comparar
                                                      entre las mejores ofertas
                                                      y tomando mejores
                                                      decisiones al momento de
                                                      alquilar un vehículo para
                                                      tus vacaciones.
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <td align="center">
                                                      <!--[if (gte mso 9)|(IE)]>
<table cellspacing="0" cellpadding="0" border="0" width="180" align="center">
<tr>
<td>
<![endif]-->
                                                      <div
                                                        style="
                                                          display: block;
                                                          width: 100%;
                                                          max-width: 180px;
                                                        "
                                                        class="cta-mobile"
                                                      >
                                                        <table
                                                          cellspacing="0"
                                                          cellpadding="0"
                                                          border="0"
                                                          width="100%"
                                                          bgcolor="black"
                                                          style="
                                                            border-radius: 100px;
                                                            -webkit-border-radius: 100px;
                                                            -moz-border-radius: 100px;
                                                          "
                                                        >
                                                          <tbody>
                                                            <tr>
                                                              <td
                                                                align="center"
                                                                height="42"
                                                                style="
                                                                  font-family: 'Poppins',
                                                                    arial;
                                                                  font-size: 18px;
                                                                  font-weight: 500;
                                                                  padding-right: 5px;
                                                                  padding-left: 5px;
                                                                "
                                                                class="cta-copy"
                                                              >
                                                                <!--																																																	
-->
                                                                <a
                                                                  href="rentytapp://app"
                                                                  target="_blank"
                                                                  style="
                                                                    display: block;
                                                                    text-decoration: none;
                                                                    color: #ffffff;
                                                                    mso-line-height-rule: exactly;
                                                                    line-height: 18px;
                                                                  "
                                                                >
                                                                  ¡Descubrir!
                                                                </a>
                                                              </td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </div>

                                                      <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                                    </td>
                                                  </tr>
                                                </table>
                                              </div>

                                              <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td
                                  style="padding-bottom: 20px"
                                  bgcolor="#f5f5f5"
                                >
                                  <table
                                    cellpadding="0"
                                    cellspacing="0"
                                    border="0"
                                    width="100%"
                                    bgcolor="#ffffff"
                                  >
                                    <tr>
                                      <td
                                        align="left"
                                        style="
                                          padding-right: 20px;
                                          padding-bottom: 50px;
                                          padding-left: 20px;
                                          padding-top: 50px;
                                        "
                                      >
                                        <table
                                          cellpadding="0"
                                          cellspacing="0"
                                          border="0"
                                          width="100%"
                                        >
                                          <tr>
                                            <td
                                              align="center"
                                              style="
                                                font-family: 'Poppins', sans-serif;
                                                font-size: 28px;
                                                color: rgb(41, 216, 132);
                                                font-weight: 600;
                                                mso-line-height-rule: exactly;
                                                line-height: 32px;
                                                padding-bottom: 10px;
                                              "
                                            >
                                              Detalles de tu cuenta
                                            </td>
                                          </tr>
                                          <tr>
                                            <td
                                              align="center"
                                              style="
                                                font-family: 'Poppins', sans-serif;
                                                font-size: 16px;
                                                color: #53565a;
                                                font-weight: 300;
                                                mso-line-height-rule: exactly;
                                                line-height: 22px;
                                                padding-bottom: 10px;
                                              "
                                            >
                                              Tu email en Rentyt:
                                            </td>
                                          </tr>
                                          <tr>
                                            <td align="center">
                                              <table
                                                cellpadding="0"
                                                cellspacing="0"
                                                width="100%"
                                                border="0"
                                              >
                                                <tr>
                                                  <td align="center">
                                                    <!--[if (gte mso 9)|(IE)]>
<table cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
<tr>
<td align="center" style="border: 4px dotted rgb(41, 216, 132);">
<![endif]-->

                                                    <!--[if (gte mso 9)|(IE)]>
<table cellpadding="0" cellspacing="0" width="528">
<tr>
<td style="display: none">
<![endif]-->
                                                    <div
                                                      style="
                                                        border: 4px dotted
                                                          rgb(41, 216, 132);
                                                        border-radius: 6px;
                                                        max-width: 528px;
                                                        width: 100%;
                                                      "
                                                    >
                                                      <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                                      <table
                                                        cellpadding="0"
                                                        cellspacing="0"
                                                        width="100%"
                                                        bgcolor="#ffffff"
                                                      >
                                                        <tr>
                                                          <td
                                                            align="center"
                                                            style="
                                                              font-family: 'Poppins',
                                                                arial;
                                                              font-size: 26px;
                                                              color: rgb(
                                                                41,
                                                                216,
                                                                132
                                                              );
                                                              font-weight: 600;
                                                              padding-bottom: 10px;
                                                              padding-top: 10px;
                                                              padding-right: 10px;
                                                              padding-left: 10px;
                                                              word-break: break-all;
                                                            "
                                                            class="account-details-username"
                                                          >
                                                            <!--
-->
                                                            <a
                                                              href="rentytapp://app"
                                                              target="_blank"
                                                              style="
                                                                color: rgb(
                                                                  41,
                                                                  216,
                                                                  132
                                                                );
                                                                text-decoration: none;
                                                              "
                                                              >${email}</a
                                                            >
                                                          </td>
                                                        </tr>
                                                      </table>

                                                      <!--[if (gte mso 9)|(IE)]>
<table>
<tr>
<td style="display: none">
<![endif]-->
                                                    </div>

                                                    <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->

                                                    <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
                                                  </td>
                                                </tr>
                                              </table>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td
                                  bgcolor="#f5f5f5"
                                  style="
                                    padding-top: 20px;
                                    padding-left: 20px;
                                    padding-right: 20px;
                                    padding-bottom: 80px;
                                  "
                                >
                                  <table
                                    cellpadding="0"
                                    cellspacing="0"
                                    border="0"
                                    width="100%"
                                    style="width: 100% !important"
                                  >
                                    <tr>
                                      <td align="center">
                                        <table
                                          border="0"
                                          cellpadding="0"
                                          cellspacing="0"
                                          width="100%"
                                          style="width: 100% !important"
                                        >
                                          <tr>
                                            <td
                                              align="center"
                                              style="padding-bottom: 20px"
                                            >
                                              <img
                                                src="https://rentyt.s3.eu-west-3.amazonaws.com/logo-rentyt-email.png"
                                                width="120"
                                                border="0"
                                                style="display: block"
                                                alt="Just-Eat&#173;.co&#173;.uk Ltd"
                                              />
                                            </td>
                                          </tr>
                                          <tr>
                                            <td
                                              align="center"
                                              style="
                                                font-family: 'Poppins',
                                                  sans-serif;
                                                font-size: 12px;
                                                color: #8d8d8d;
                                                padding-bottom: 20px;
                                              "
                                              colspan="5"
                                            >
                                              <span
                                                style="
                                                  color: rgb(41, 216, 132);
                                                  text-decoration: none;
                                                "
                                                >Rentyt App</span
                                              >
                                            </td>
                                          </tr>
                                          <tr>
                                            <td
                                              align="center"
                                              style="
                                                font-family: 'Poppins',
                                                  sans-serif;
                                                font-size: 12px;
                                                color: #8d8d8d;
                                                padding-bottom: 20px;
                                                mso-line-height-rule: exactly;
                                                line-height: 22px;
                                              "
                                              colspan="5"
                                            >
                                              Recuerda, Rentyt nunca, te enviará
                                              un correo electrónico pidiéndote
                                              tus datos bancarios o de la
                                              tarjeta.<br />Entonces, si recibes
                                              algo como esto, bórralo. Te
                                              recomendamos que cambie tu
                                              contraseña regularmente también.
                                            </td>
                                          </tr>
                                          <tr>
                                            <td
                                              align="center"
                                              style="
                                                font-family: 'Poppins',
                                                  sans-serif;
                                                font-size: 12px;
                                                color: #8d8d8d;
                                                padding-bottom: 20px;
                                                mso-line-height-rule: exactly;
                                                line-height: 22px;
                                              "
                                              colspan="5"
                                            >
                                              Copyright © ${new Date().getFullYear()} Rentyt
                                              <span>|</span> Santo Domingo,
                                              República Dominicana
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>

                                    <!-- [footer content: ends] -->
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>

          <!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
        </td>
      </tr>
    </table>
  </body>
</html>
<html>
  <p></p>
</html>

  `;
};

export default t;
