import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const lastViewSchema = new mongoose.Schema(
  {
    adsID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ads",
      required: true,
    },

    userID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      required: true,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface ILastView extends Document {
  adsID: string;
  userID: String;
  created_at: Date;
  updated_at: Date;
}

export default mongoose.model<ILastView>("lastView", lastViewSchema);
