import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const cuponesSchema = new mongoose.Schema({
  clave: { type: String, unique: true, required: true },
  descuento: {
    type: Number,
    required: true,
  },
  tipo: {
    type: String,
    enum: ["porcentaje", "dinero"],
    default: "porcentaje",
    required: true,
  },
  usage: {
    type: Number,
    default: 1,
  },
  expire: {
    type: Date,
    default: new Date(),
  },
  exprirable: {
    type: Boolean,
    default: false,
  },
  user: {
    type: String,
  },
  description: {
    type: String,
  },

  private: {
    type: Boolean,
    default: false,
  },

  uniqueCar: {
    type: Boolean,
    default: false,
  },

  car: {
    type: String,
  },
});

export interface ICupon extends Document {
  clave: string;
  tipo: String;
  descuento: string;
  usage: number;
  expire: any;
  exprirable: boolean;
  user: string;
  description: string;
  uniqueCar: boolean;
  car: string;
  private: boolean;
}

export default mongoose.model<ICupon>("cupones", cuponesSchema);
