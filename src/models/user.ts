import mongoose, { Document } from "mongoose";
import bcrypt from "bcryptjs";

mongoose.Promise = global.Promise;

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      text: true,
    },
    lastName: {
      type: String,
      required: true,
      text: true,
    },

    description: {
      type: String,
      text: true,
    },

    email: {
      type: String,
      required: true,
      unique: true,
    },

    city: {
      type: String,
    },

    avatar: {
      type: String,
      default:
        "https://internal-image-wilbby.s3.eu-west-3.amazonaws.com/defaultAvatar.jpg",
    },

    password: {
      type: String,
      required: true,
      unique: true,
    },

    termAndConditions: {
      type: Boolean,
      required: true,
      default: false,
    },

    isSocial: {
      type: Boolean,
    },

    verifyPhone: {
      type: Boolean,
      default: false,
    },

    isAvalible: {
      type: Boolean,
      default: true,
    },

    phone: {
      type: String,
    },

    StripeID: {
      type: String,
    },

    OnesignalID: {
      type: String,
    },

    verified: {
      type: Boolean,
      default: false,
    },

    myCategory: {
      type: [String],
      default: [],
    },

    lastSeen: {
      type: Date,
      require: true,
      default: new Date(),
    },
    rating: {
      type: String,
      default: "0.0",
    },
    socialNetworks: {
      type: String,
    },

    location: {
      type: mongoose.Schema.Types.Mixed,
    },
    status: {
      type: String,
    },
    verification: {
      type: mongoose.Schema.Types.Mixed,
    },
    technicalData: {
      type: mongoose.Schema.Types.Mixed,
    },

    documents: {
      type: mongoose.Schema.Types.Mixed,
    },

    additionalAddress: {
      type: String,
    },

    isAdmin: {
      type: Boolean,
      default: false,
      require: true,
    },

    source: {
      type: [mongoose.Schema.Types.Mixed],
    },

    lastLogin: {
      type: mongoose.Schema.Types.Mixed,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IUser extends Document {
  name: string;
  lastName: string;
  email: string;
  password: string;
  verifyPhone: boolean;
  OnesignalID: any;
  phone: string;
  isAvalible: boolean;
  verified: boolean;
  myCategory: string[];
  lastSeen: Date;
  rating: string;
  description: string;
  location: any;
  avatar: string;
  city: string;
  additionalAddress: string;
  isAdmin: boolean;
  source: any[];
  lastLogin: any;
}

// hashear los password antes de guardar
userSchema.pre<IUser>("save", function (next) {
  // Si el password no esta hasheado...
  if (!this.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(this.password, salt, (err, hash) => {
      if (err) return next(err);
      this.password = hash;
      next();
    });
  });
});

export default mongoose.model<IUser>("user", userSchema);
