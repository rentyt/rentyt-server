import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const orderSchema = new mongoose.Schema(
  {
    channelOrderDisplayId: { type: Number, unique: true, required: true },
    pickupDate: { type: mongoose.Schema.Types.Mixed, required: true },
    deliveryDate: { type: mongoose.Schema.Types.Mixed, required: true },
    customer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      required: true,
    },
    owner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      required: true,
    },
    car: { type: mongoose.Schema.Types.ObjectId, ref: "ads", required: true },
    orderIsAlreadyPaid: { type: Boolean, default: false, required: true },
    payment: { type: mongoose.Schema.Types.Mixed, required: true },
    note: { type: String, required: true },
    IntegerValue: {
      type: Number,
      enum: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
      default: 10,
    },
    paymentMethod: {
      type: String,
      enum: [
        "Paypal",
        "Tarjeta de credito",
        "Efectivo",
        "Apple Pay",
        "Google Pay",
      ],
      required: true,
    },
    cupon: { type: mongoose.Schema.Types.ObjectId, ref: "cupones" },
    statusProcess: { type: mongoose.Schema.Types.Mixed, required: true },
    pagoPaypal: { type: mongoose.Schema.Types.Mixed },
    stripePaymentIntent: { type: mongoose.Schema.Types.Mixed },
    status: {
      type: String,
      enum: [
        "Pendiente de pago",
        "Nueva",
        "Confirmada",
        "Rechazado",
        "Recogido",
        "Entregado",
        "Finalizada",
        "Devuelto",
      ],
      default: "Pendiente de pago",
      required: true,
    },

    holdedPartnerID: {
      type: String,
    },

    invoiceUrl: {
      type: String,
    },

    selectdDay: {
      type: [mongoose.Schema.Types.Mixed],
      required: true,
    },
    babyChair: {
      type: [mongoose.Schema.Types.Mixed],
    },
    extraEquipament: {
      type: [mongoose.Schema.Types.Mixed],
    },
    airplanePickUp: { type: Boolean, default: false, required: true },
    instantBooking: { type: Boolean, default: false, required: true },
    airPort: {
      type: String,
    },
    source: {
      type: mongoose.Schema.Types.Mixed,
    },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

orderSchema.virtual("descuento", {
  ref: "cupones",
  localField: "cupon",
  foreignField: "_id",
  justOne: true,
});

export interface IOrders extends Document {
  channelOrderDisplayId: number;
  pickupDate: any;
  deliveryDate: any;
  customer: string;
  owner: string;
  car: string;
  orderIsAlreadyPaid: Boolean;
  payment: any;
  note: string;
  IntegerValue: number;
  paymentMethod: string;
  cupon: string;
  statusProcess: any;
  pagoPaypal: any;
  stripePaymentIntent: any;
  status: string;
  holdedPartnerID: string;
  invoiceUrl: string;
  created_at: Date;
  updated_at: Date;
  airplanePickUp: boolean;
  airPort: string;
  source: any;
}

export default mongoose.model<IOrders>("newOrder", orderSchema);
