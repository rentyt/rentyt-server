import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const ratingSchema = new mongoose.Schema(
  {
    owner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      required: true,
    },
    customer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      required: true,
    },

    car: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ads",
      required: true,
    },

    order: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "newOrder",
      required: true,
    },

    rating: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },
    comment: {
      type: String,
    },
    reply: {
      type: mongoose.Schema.Types.Mixed,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IRating extends Document {
  owner: string;
  customer: String;
  car: string;
  order: string;
  rating: any;
  comment: string;
  reply: any;
}

export default mongoose.model<IRating>("rating", ratingSchema);
