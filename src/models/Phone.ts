import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const phoneSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    text: true,
  },
  code: {
    type: String,
    required: true,
    text: true,
  },
  dial_code: {
    type: String,
    required: true,
    text: true,
  },
  flag: {
    type: String,
    required: true,
  },
});

export interface IPhone extends Document {
  name: string;
  code: string;
  dial_code: string;
  flag: string;
}

export default mongoose.model<IPhone>("phone", phoneSchema);
