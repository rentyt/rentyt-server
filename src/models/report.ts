import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const reportSchema = new mongoose.Schema(
  {
    coment: {
      type: String,
      required: true,
      text: true,
    },
    razon: {
      type: String,
      required: true,
      text: true,
    },

    type: {
      type: String,
      required: true,
      text: true,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      required: true,
    },
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IReport extends Document {
  coment: string;
  razon: string;
  type: string;
  user: string;
  id: string;
  created_at: Date;
}

export default mongoose.model<IReport>("report", reportSchema);
