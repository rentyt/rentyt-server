import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const selectMarksSchema = new mongoose.Schema({
  label: {
    type: String,
    required: true,
    text: true,
  },
  value: {
    type: Number,
    required: true,
  },
  models: {
    type: [mongoose.Schema.Types.Mixed],
  },
});

export interface IMarks extends Document {
  label: string;
  value: number;
  models: any[];
}

export default mongoose.model<IMarks>("selectMark", selectMarksSchema);
