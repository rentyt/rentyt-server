import mongoose, { Document } from "mongoose";
import { IUser } from "./user";

mongoose.Promise = global.Promise;

const blogPostSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      text: true,
    },
    short_description: {
      type: String,
      required: true,
      text: true,
    },
    author: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },
    image: {
      type: String,
      required: true,
    },
    tags: {
      type: [String],
      required: true,
      text: true,
    },
    slug: {
      type: String,
      required: true,
      text: true,
    },
    content: {
      type: String,
      required: true,
      text: true,
    },
    like: {
      type: [String],
    },

    related: {
      type: String,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IPost extends Document {
  title: string;
  short_description: string;
  author: IUser;
  image: string;
  tags: string[];
  slug: string;
  content: string;
  like: string[];
  related: string;
}

export default mongoose.model<IPost>("blogpost", blogPostSchema);
