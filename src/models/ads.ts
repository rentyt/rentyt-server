import mongoose, { Document } from "mongoose";

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

mongoose.Promise = global.Promise;

const PointScema = new mongoose.Schema(
  {
    type: {
      type: String,
      enum: ["Point", "LineString", "Polygon"],
      default: "Point",
    },
    coordinates: {
      type: [Number],
      index: "2dsphere",
    },
  },
  { _id: false }
);

const adsSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      text: true,
    },

    car: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },
    instantBooking: {
      type: Boolean,
      default: false,
    },

    inService: {
      type: Boolean,
      default: false,
    },

    popular: {
      type: Boolean,
      default: false,
    },

    minDays: {
      type: Number,
    },

    gestionCost: {
      type: Number,
    },

    anticipation: {
      type: [mongoose.Schema.Types.Mixed],
    },

    offerts: {
      type: [mongoose.Schema.Types.Mixed],
    },

    images: {
      type: [mongoose.Schema.Types.Mixed],
      required: true,
    },

    description: {
      type: String,
      required: true,
    },

    owner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
      required: true,
    },

    location: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },

    details: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },

    equipemnet: {
      type: [mongoose.Schema.Types.Mixed],
    },

    ExtraEquipment: {
      type: [mongoose.Schema.Types.Mixed],
    },
    babyChairs: {
      type: [mongoose.Schema.Types.Mixed],
    },
    preferences: {
      type: [mongoose.Schema.Types.Mixed],
    },

    prices: {
      type: mongoose.Schema.Types.Mixed,
      required: true,
    },

    source: {
      type: mongoose.Schema.Types.Mixed,
    },

    availability: {
      type: mongoose.Schema.Types.Mixed,
    },

    busyDays: {
      type: mongoose.Schema.Types.Mixed,
    },

    city: {
      type: String,
      required: true,
      default: "República Dominicana",
    },

    visible: {
      type: Boolean,
      default: true,
    },

    rating: {
      type: String,
      default: "0.0",
    },

    status: {
      type: String,
      enum: ["Publish", "In review", "Succees", "Rejected", "Deleted", "Draft"],
      default: "Draft",
      required: true,
    },

    statusProcess: { type: [mongoose.Schema.Types.Mixed], required: true },

    offertDay: {
      type: Boolean,
      default: false,
    },

    offertMon: {
      type: Boolean,
      default: false,
    },

    airPortPickup: {
      type: mongoose.Schema.Types.Mixed,
      default: false,
      required: true,
    },

    airPorts: {
      type: mongoose.Schema.Types.Mixed,
      default: [],
    },

    message: {
      type: String,
      default: null,
    },

    display_id: {
      type: String,
      default: `RT-${Number.parseInt(getRandomArbitrary(1000, 9999999))}`,
      require: true,
      unique: true,
    },

    geometry: PointScema,
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IAds extends Document {
  car: any;
  instantBooking: boolean;
  inService: boolean;
  minDays: number;
  gestionCost: number;
  anticipation: any[];
  offerts: any[];
  owner: string;
  images: any[];
  description: string;
  location: any;
  details: any;
  equipemnet: any[];
  ExtraEquipment: any[];
  babyChairs: any[];
  preferences: any[];
  prices: any;
  availability: any;
  busyDays: any;
  created_at: Date;
  updated_at: Date;
  popular: boolean;
  visible: boolean;
  rating: string;
  status: string;
  statusProcess: any[];
  offertDay: boolean;
  offertMon: boolean;
  airPortPickup: any;
  airPorts: any[];
  message: string;
  display_id: string;
  source: any;
}

export default mongoose.model<IAds>("ads", adsSchema);
