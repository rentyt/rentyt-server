import { sendNotifications } from "../router/Notification/Notification";

export const PushNotification = async (data: any) => {
  const renderCont = () => {
    if (data.text && !data.image) {
      return data.text;
    } else if (data.text && data.image) {
      return data.text;
    } else if (!data.text && data.image) {
      return "Imagen";
    }
  };

  const imagen = data.image ? data.image : data.user.avatar;

  const dataNotification = {
    navigate: true,
    router: "ChatScreen",
    extraData: true,
    data: data,
  };

  sendNotifications(
    data.OnesignalID,
    renderCont(),
    renderCont(),
    data.user.name,
    data.user.name,
    imagen,
    dataNotification
  );
};
