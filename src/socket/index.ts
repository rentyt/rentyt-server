import { UpdateUser } from "./utils";
import { PushNotification } from "./sendNotification";

export const SocketIO = (io: any) => {
  let connectedUsers = {};

  io.on("connection", (socket) => {
    socket.on("online_user", async (data) => {
      connectedUsers[data.id] = socket.id;
    });

    socket.on("send_notification", async (data) => {
      PushNotification(data);
    });

    socket.on("typing", (data) => {
      io.to(connectedUsers[data.receptor]).emit("typing", data);
    });

    socket.on("disconnect", () => {});
  });
};
