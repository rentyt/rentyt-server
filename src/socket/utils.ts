import userSchema, { IUser } from "../models/user";

export const UpdateUser = (id: string) => {
  userSchema.findOneAndUpdate(
    { _id: id },
    { lastSeen: new Date() },
    { new: true },
    (error, _usuario: IUser) => {
      console.log("done lastseen save", _usuario._id);
    }
  );
};
