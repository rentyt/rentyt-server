import dotenv from "dotenv";
import userSchema from "../models/user";

dotenv.config({ path: "variables.env" });
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

export const createStripeClient = async (name, email, id) => {
  await stripe.customers.create(
    {
      name: name,
      email: email,
      description: "Clientes de Rentyt",
    },
    function (err: any, customer: any) {
      userSchema.findOneAndUpdate(
        { _id: id },
        {
          $set: {
            StripeID: customer.id,
          },
        },
        //@ts-ignore
        (err: any, customer: any) => {
          if (err) {
            console.log(err);
          }
        }
      );
    }
  );
};
