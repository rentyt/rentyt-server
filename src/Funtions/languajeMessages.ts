export const messageLang = () => {
  return {
    Nueva: {
      en: "Great the owner has received your reservation soon you will receive news",
      es: "Genial el propietario ha recibido tu reserva pronto recibirás noticias",
    },
    Confirmada: {
      en: "This is progressing the owner has confirmed your reservation you are almost sitting in the vehicle",
      es: "Esto avanza el propietario ha confirmado tu reserva ya casi estas sentando el en vehículo",
    },
    Rechazado: {
      en: "Oops the owner has rejected your reservation, we will issue a refund shortly",
      es: "¡Ups el propietario ha rechazado tu reserva, en breve emitiremos una devolución!",
    },
    Entregado: {
      en: "The owner has given you the vehicle we hope you enjoy it",
      es: "El propietario te ha entregado el vehículo esperamos que lo disfrute",
    },
    Recogido: {
      en: "You have delivered the vehicle to the owner and everything is in perfect condition",
      es: "Has entregado el vehículo al propietario y todo está en perfecto estado",
    },

    Finalizada: {
      en: "You have valued the service of the owner, thank you very much",
      es: "Has valorado el servicio del propietario, Muchas gracias",
    },
    Devuelto: {
      en: "We have issued a refund of your reservation, it will soon be reflected in your bank account",
      es: "Hemos emitido una devolución de tu reserva pronto se vera reflejado en tu cuenta bancaria",
    },
  };
};

export const messageDataOwner = () => {
  return {
    Nueva: {
      en: "Vehicle:",
      es: "Vehículo:",
    },
    Confirmada: {
      en: "Vehicle:",
      es: "Vehículo:",
    },

    Finalizada: {
      en: "The client has valued your service you are doing a good job",
      es: "El cliente ha valorado tu servicio estás haciendo un buen trabajo",
    },
  };
};

export const messagePublishVehicle = () => {
  return {
    Inreview: {
      en: "The Rentyt team is reviewing your ad shortly you will hear from us",
      es: "El equipo de Rentyt esta revisado tu anuncio en breve tendrás noticias de nosotros",
    },
    Rejected: {
      en: "Your ad has been rejected we will send you an email with the details",
      es: "Tu anuncio ha sido rechazado te enviaremos un correo con los detalles",
    },

    Succees: {
      en: "Your ad has been published and is now available in the Rentyt app",
      es: "Tu anuncio ha sido publicado y ya esta disponible en la app de Rentyt",
    },
  };
};
