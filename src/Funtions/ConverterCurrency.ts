var rp = require("request-promise");
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });

const apiKey = process.env.FASRFOREX_KEY;

export const converterCurrent = async (amount: number) => {
  return await rp(
    `https://api.fastforex.io/convert?from=DOP&to=USD&amount=${amount}&api_key=${apiKey}`
  );
};
