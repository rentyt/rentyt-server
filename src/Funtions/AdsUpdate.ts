import AdsSchema, { IAds } from "../models/ads";
import userSchema from "../models/user";
import ratingSchema from "../models/rating";
import orderChema from "../models/order";
import { RatingCalculator } from "./RatingCalculartorCar";
import { RatingCalculatorUser } from "./RatingCalculartorUser";

export const UpdateBusyCar = async (id: string, busyDays: any) => {
  AdsSchema.findOneAndUpdate(
    { _id: id },
    {
      busyDays: busyDays,
    },
    { upsert: true },
    () => {
      console.log("Car Update busy Day");
    }
  );
};

export const UpdateCarStatus = async (id: string) => {
  AdsSchema.findOneAndUpdate(
    { _id: id },
    {
      status: "In review",
      $push: {
        statusProcess: { status: "In review", date: new Date() },
      },
    },
    { upsert: true },
    () => {
      console.log("Car Update Status");
    }
  );
};

export const updateCarRating = async (id: string) => {
  const ratings = await ratingSchema.find({ car: id });
  const averageRating = RatingCalculator(ratings);

  AdsSchema.findOneAndUpdate(
    { _id: id },
    {
      rating: averageRating > 0 ? `${averageRating.toFixed(1)}` : "0.0",
    },
    { upsert: true },
    () => {
      console.log("Car Rating update");
    }
  );
};

export const updateUserRating = async (id: string) => {
  const ratings = await ratingSchema.find({ owner: id });
  const averageRating = RatingCalculatorUser(ratings);

  userSchema.findOneAndUpdate(
    { _id: id },
    {
      rating: averageRating > 0 ? `${averageRating.toFixed(1)}` : "0.0",
    },
    { upsert: true },
    () => {
      console.log("User Rating update");
    }
  );
};

export const UpdateOrderStatus = async (id: string) => {
  orderChema.findOneAndUpdate(
    { _id: id },
    {
      status: "Finalizada",
      $push: {
        statusProcess: { status: "Finalizada", date: new Date() },
      },
    },
    { upsert: true },
    () => {
      console.log("Order Rating Update Status");
    }
  );
};

export const updateUserLogin = async (id: string, input: any) => {
  userSchema.findOneAndUpdate(
    { _id: id },
    {
      $push: {
        source: input,
      },
      lastLogin: input,
    },
    { upsert: true },
    () => {
      console.log("User Rating update");
    }
  );
};

export const updateCarPosition = async (input: IAds) => {
  AdsSchema.findOneAndUpdate(
    { _id: input._id },
    {
      geometry: {
        type: "Point",
        coordinates: [
          input.location.geometry.location.lat,
          input.location.geometry.location.lng,
        ],
      },
    },
    { new: true },
    () => {
      console.log("Car position update");
    }
  );
};
