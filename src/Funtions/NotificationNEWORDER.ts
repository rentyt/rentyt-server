import { sendNotifications } from "../router/Notification/Notification";
import { IOrders } from "../models/order";
import adsSchema from "../models/ads";
import userSchema from "../models/user";
import { titleDataOwner } from "./languaTitles";
import { messageDataOwner } from "./languajeMessages";

export const pushNotificationNewOrder = async (
  OnesignalID: string,
  status: string,
  order: IOrders
) => {
  const car = await adsSchema.findOne({ _id: order.car });
  const user = await userSchema.findOne({ _id: order.customer });
  let message_es = "";
  let message_en = "";
  let title_es = "";
  let title_en = "";
  let image = car.images[0].uri;
  const dataNotification = {
    navigate: true,
    router: "MyRequestDetails",
    data: order._id,
  };

  switch (status) {
    case "Nueva":
      message_es = `${messageDataOwner().Nueva.es} ${car.car.marker} ${
        car.car.model
      } ${car.car.year} de ${user.name} ${user.lastName}`;
      message_en = `${messageDataOwner().Nueva.en} ${car.car.marker} ${
        car.car.model
      } ${car.car.year} from ${user.name} ${user.lastName}`;
      title_es = titleDataOwner().Nueva.es;
      title_en = titleDataOwner().Nueva.en;
      break;
    case "Confirmada":
      message_es = `${messageDataOwner().Confirmada.es} ${car.car.marker} ${
        car.car.model
      } ${car.car.year} de ${user.name} ${user.lastName}`;
      message_en = `${messageDataOwner().Confirmada.en} ${car.car.marker} ${
        car.car.model
      } ${car.car.year} from ${user.name} ${user.lastName}`;
      title_es = titleDataOwner().Confirmada.es;
      title_en = titleDataOwner().Confirmada.en;
      break;
    case "Finalizada":
      message_es = messageDataOwner().Finalizada.es;
      message_en = messageDataOwner().Finalizada.en;
      title_es = titleDataOwner().Finalizada.es;
      title_en = titleDataOwner().Finalizada.en;
      break;
  }

  sendNotifications(
    OnesignalID,
    message_es,
    message_en,
    title_es,
    title_en,
    image,
    dataNotification
  );
};
