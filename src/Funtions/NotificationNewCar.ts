import { sendNotifications } from "../router/Notification/Notification";
import { titlePublishVehicle } from "./languaTitles";
import { messagePublishVehicle } from "./languajeMessages";

export const pushNotificationNewCar = async (
  OnesignalID: string,
  status: string,
  car: any
) => {
  let message_es = "";
  let message_en = "";
  let title_es = "";
  let title_en = "";
  let image = car.images[0].uri;
  const dataNotification = {
    navigate: true,
    router: "Details",
    data: car._id,
  };

  switch (status) {
    case "Inreview":
      message_es = `${messagePublishVehicle().Inreview.es}`;
      message_en = `${messagePublishVehicle().Inreview.en}`;
      title_es = titlePublishVehicle().Inreview.es;
      title_en = titlePublishVehicle().Inreview.en;
      break;
    case "Rejected":
      message_es = `${messagePublishVehicle().Rejected.es}`;
      message_en = `${messagePublishVehicle().Rejected.en}`;
      title_es = titlePublishVehicle().Rejected.es;
      title_en = titlePublishVehicle().Rejected.en;
      break;
    case "Succees":
      message_es = messagePublishVehicle().Succees.es;
      message_en = messagePublishVehicle().Succees.en;
      title_es = titlePublishVehicle().Succees.es;
      title_en = titlePublishVehicle().Succees.en;
      break;
  }

  sendNotifications(
    OnesignalID,
    message_es,
    message_en,
    title_es,
    title_en,
    image,
    dataNotification
  );
};
