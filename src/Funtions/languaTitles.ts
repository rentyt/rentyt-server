export const titleData = () => {
  return {
    Nueva: {
      en: "Booking received",
      es: "Reserva recibida",
    },
    Confirmada: {
      en: "Your booking has been confirmed",
      es: "Tu reserva ha sido confirmada",
    },
    Rechazado: {
      en: "Booking declined",
      es: "Reserva rechazada",
    },
    Recogido: {
      en: "Delivered vehicle",
      es: "Vehículo entregado",
    },
    Entregado: {
      en: "Picked up vehicle",
      es: "Vehículo recogido",
    },
    Finalizada: {
      en: "Booking completed",
      es: "Reserva finalizada",
    },
    Devuelto: {
      en: "Booking returned",
      es: "Reserva devuelta",
    },
  };
};

export const titleDataOwner = () => {
  return {
    Nueva: {
      en: "You have received a new booking",
      es: "Has recibido una nueva reserva",
    },
    Confirmada: {
      en: "The reservation has been confirmed automatically",
      es: "La reserva ha sido confirmada automáticamente",
    },

    Finalizada: {
      en: "The client has valued your service",
      es: "El cliente ha valorado tu servicio",
    },
  };
};

export const titlePublishVehicle = () => {
  return {
    Inreview: {
      en: "Ad in review",
      es: "Anuncio en revisión",
    },
    Rejected: {
      en: "Ad rejected",
      es: "Anuncio rechazado",
    },

    Succees: {
      en: "Ad published",
      es: "Anuncio publicado",
    },
  };
};
