import { sendNotifications } from "../router/Notification/Notification";
import { IOrders } from "../models/order";
import adsSchema from "../models/ads";
import { titleData } from "./languaTitles";
import { messageLang } from "./languajeMessages";

export const pushNotificationOrderProccess = async (
  OnesignalID: string,
  status: string,
  order: IOrders
) => {
  const car = await adsSchema.findOne({ _id: order.car });
  let message_es = "";
  let message_en = "";
  let title_es = "";
  let title_en = "";
  let image = car.images[0].uri;
  const dataNotification = {
    navigate: true,
    router: "DetailsOrders",
    data: order._id,
  };

  switch (status) {
    case "Nueva":
      message_es = messageLang().Nueva.es;
      message_en = messageLang().Nueva.en;
      title_es = titleData().Nueva.es;
      title_en = titleData().Nueva.en;
      break;
    case "Confirmada":
      message_es = messageLang().Confirmada.es;
      message_en = messageLang().Confirmada.en;
      title_es = titleData().Confirmada.es;
      title_en = titleData().Confirmada.en;
      break;
    case "Recogido":
      message_es = messageLang().Recogido.es;
      message_en = messageLang().Recogido.en;
      title_es = titleData().Recogido.es;
      title_en = titleData().Recogido.en;
      break;
    case "Entregado":
      message_es = messageLang().Entregado.es;
      message_en = messageLang().Entregado.en;
      title_es = titleData().Entregado.es;
      title_en = titleData().Entregado.en;
      break;
    case "Finalizada":
      message_es = messageLang().Finalizada.es;
      message_en = messageLang().Finalizada.en;
      title_es = titleData().Finalizada.es;
      title_en = titleData().Finalizada.en;
      break;
    case "Devuelto":
      message_es = messageLang().Devuelto.es;
      message_en = messageLang().Devuelto.en;
      title_es = titleData().Devuelto.es;
      title_en = titleData().Devuelto.en;
      break;
    case "Rechazado":
      message_es = messageLang().Rechazado.es;
      message_en = messageLang().Rechazado.en;
      title_es = titleData().Rechazado.es;
      title_en = titleData().Rechazado.en;
      break;
  }

  sendNotifications(
    OnesignalID,
    message_es,
    message_en,
    title_es,
    title_en,
    image,
    dataNotification
  );
};
